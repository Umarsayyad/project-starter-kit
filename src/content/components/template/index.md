---

layout: custom
group: components
path: /components/template/
title: Template
status: Complete
component: Tabs
heading: Tabs
role: main
description: Default Tabs for Components
tabs: [Usage, Code, Style]

---
