import React, { Component } from 'react'
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem, Collapse } from 'reactstrap'

class DropExample extends Component {
  constructor(props) {
    super(props)
    this.toggle = this.toggle.bind(this)
    this.state = {
      collapse: false,
      icon: 'btn-icon__right icon prs-icon-triangle-down',
    }
  }

  toggle() {
    this.setState({
      collapse: !this.state.collapse,
    })
    if (this.state.collapse) {
      this.setState({ icon: 'btn-icon__right icon prs-icon-triangle-down' })
    } else {
      this.setState({ icon: 'btn-icon__right icon prs-icon-triangle-up' })
    }
  }

  render() {
    const ddClass = this.props.className
    const ddColor = this.props.color
    return (
      <Dropdown isOpen={this.state.collapse}>
        <DropdownToggle
          className={ddClass}
          color="primary"
          onClick={this.toggle}
        >
          MENU{' '}
          <span>
            <i className= {this.state.icon} />
          </span>
        </DropdownToggle>
        <DropdownMenu>
          <DropdownItem header>Header</DropdownItem>
          <DropdownItem>Some Action</DropdownItem>
          <DropdownItem disabled>Action (disabled)</DropdownItem>
          <DropdownItem divider />
          <DropdownItem>Foo Action</DropdownItem>
          <DropdownItem>Bar Action</DropdownItem>
          <DropdownItem>Quo Action</DropdownItem>
        </DropdownMenu>
      </Dropdown>
    )
  }
}

export default DropExample