import React, {Component} from 'react';
import {FormGroup, Label, Input} from 'reactstrap';

class ItemFilter extends Component {

    render() {
        const heading = this.props.heading;

        return (
            <FormGroup className="filter-group">
                <FormGroup check>
                    <Label className='circle-check' check>
                        <Input type="checkbox" name="check1"/> {' '}
                        <span className='p-1'>Filter Item</span>
                        <span className='p-1'>#,###</span>
                    </Label>
                </FormGroup>
                <FormGroup check>
                    <Label className='circle-check' check>
                        <Input type="checkbox" name="check2"/> {' '}
                        <span className='p-1'>Filter Item</span>
                        <span className='p-1'>#,###</span>
                    </Label>
                </FormGroup>
                <FormGroup check disabled>
                    <Label className='circle-check' check>
                        <Input type="checkbox" name="check3" disabled/> {' '}
                        <span className='p-1'>Filter Item</span>
                        <span className='p-1'>#,###</span>
                    </Label>
                </FormGroup>
            </FormGroup>
        );
    }
}

export default ItemFilter;
