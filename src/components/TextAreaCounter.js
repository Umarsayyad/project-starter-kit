import React, {Component} from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'

export default class TextAreaCounter extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showPass: false,
            remaining: 120,
            max: 120,
            reachedMax: false
        }

        this.handleChange = this
            .handleChange
            .bind(this);
    }

    componentDidMount() {
        this.setState({max: this.props.maxLength})
        this.setState({remaining: this.props.maxLength})

    }

    handleChange(event) {
        this.setState({
            remaining: this.props.maxLength - event.target.value.length
        });
        console.log()
        if (this.state.reachedMax && event.target.value.length < this.state.max) {
            let newLength = this.state.max - event.target.value.length;
            this.setState({remaining: newLength});
            this.setState({reachedMax: false})
        } else if (this.state.remaining < 1) {
            event.target.value = event
                .target
                .value
                .slice(0, -1);
            this.setState({reachedMax: true})
            this.setState({remaining: 0})
        } else {
            let newLength = this.state.max - event.target.value.length;
            this.setState({remaining: newLength});

        }
    }

    render() {
        const counterClass = classNames("form-text mt-8", {'text-danger': this.state.reachedMax});
        const {remaining, reachedMax} = this.state
        return (

            <div>
                <label htmlFor="exampleFormControlTextarea1">Text Area with counter</label>
                <textarea
                    className="form-control"
                    id="exampleFormControlTextarea1"
                    placeholder='Text area with characters remaining counter'
                    rows={this.props.rows}
                    onChange={this.handleChange}></textarea>
                <small id="textCounter" className={counterClass}>{remaining}{' '}characters remaining</small>
            </div>
        )
    }
}

TextAreaCounter.propTypes = {
    rows: PropTypes.string,
    maxLength: PropTypes.number
};
