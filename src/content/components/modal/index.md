---
layout: custom
group: components
path: /components/modal/
title: Modal
status: Complete
component: Modal
heading: Modal
role: main
description: Modals
tabs: [Usage, Code, Style]
---
