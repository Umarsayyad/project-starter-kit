---
layout: custom
group: elements
path: /elements/links/code
heading: Links
title: Links
description: Links are simple controls for executing specific on-page actions and code. Buttons are most commonly used in forms, dialogs, and modals to facilitate required user actions.
tabs: ['Usage', 'Code', 'Style']
---

#LINKS
