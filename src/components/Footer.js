import React from 'react'
import {Link} from 'gatsby'

const Footer = () => (
  <footer className="prism-footer" role="contentinfo">
    <ul className="prism-footer-nav">
      <li className="prism-footer-nav__item">
        <Link className="prism-footer-nav__link" to="/about">
          About
        </Link>
      </li>
      <li className="prism-footer-nav__item">
        <Link className="prism-footer-nav__link" to="/roadmap">
          Roadmap
        </Link>
      </li>
      <li className="prism-footer-nav__item">
        <Link className="prism-footer-nav__link" to="/history">
          Release History
        </Link>
      </li>
      <li className="prism-footer-nav__item">
        <Link className="prism-footer-nav__link" to="/contribute">
          Contribute
        </Link>
      </li>
    </ul>
  </footer>
)

export default Footer
