import React from 'react'
import {graphql} from 'gatsby'
import Fonts from '../styles/prs-styles/fonts/Manheim.ttf'
import Layout from '../components/layout'
import PageHeader from '../components/PageHeader'

export default function Index({data}) {
  const {markdownRemark: post} = data
  return (
    <Layout>

      <PageHeader
        title="Umar's project jump starter"
        description="Umar's project jump starter for your convience"/>
      <div className="l-container">
        <div
          className="prism-text-passage"
          dangerouslySetInnerHTML={{
          __html: post.html
        }}/>
      </div>
    </Layout>
  )
}

export const indexQuery = graphql `
  query IndexQuery($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      frontmatter {
        path
        tags
        kicker
        title
        description
        status
        group
      }
    }
  }
`
