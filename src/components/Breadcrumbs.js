import React from 'react'
import {Link} from 'gatsby'

const Breadcrumbs = ({group, subgroup}) => {
  return (
    <ol className="prism-breadcrumbs">
      <li className="prism-breadcrumbs__item">
        <Link className="prism-breadcrumbs__link" to={'/' + group}>
          {group}
        </Link>
      </li>

      {subgroup && (
        <li className="prism-breadcrumbs__item">
          <Link className="prism-breadcrumbs__link" to={'/' + group + '/' + subgroup}>
            {subgroup}
          </Link>
        </li>
      )}
    </ol>
  )
}

export default Breadcrumbs
