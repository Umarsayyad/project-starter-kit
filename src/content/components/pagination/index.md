---
layout: custom
group: components
path: /components/pagination/
title: Pagination
status: Complete
component: Pagination
heading: Pagination
role: main
description: Pagination
tabs: [Usage, Code, Style]
---
