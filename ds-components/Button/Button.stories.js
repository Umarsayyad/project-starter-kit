import React from 'react';
import {setAddon, storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';
import {linkTo} from '@storybook/addon-links';
import {withKnobs, text, boolean, number} from '@storybook/addon-knobs';
import JSXAddon from 'storybook-addon-jsx';
import {withLiveEditScope} from 'storybook-addon-react-live-edit';

import Button from './Button.js';
import '../../src/styles/style.scss';

setAddon(JSXAddon);

const stories = storiesOf('Button', module);
stories.addDecorator(withKnobs);

stories.addDecorator(withLiveEditScope({Button}))
stories.addLiveSource('with text', () => <Button
                disabled={boolean('Disabled', false)}
                text={text('')}
                outline={boolean('outline', false)}
                styleModifier='btn-primary'
                color='primary'
                onClick={action('clicked')}>Hello Button</Button>
)