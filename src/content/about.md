---
layout: page
title: About the design system
description: Bacon ipsum dolor amet bresaola tri-tip doner ground round brisket. Short loin meatball alcatra kielbasa, ribeye kevin jowl shoulder brisket pork loin tail landjaeger. Alcatra strip steak cupim, meatloaf picanha pastrami boudin. Chicken boudin beef, sirloin pastrami swine picanha landjaeger beef ribs tri-tip strip steak meatloaf prosciutto ball tip.

path: /about
---

Check out [MailChimp's pattern library about page](http://ux.mailchimp.com/patterns/about) for inspiration.
