import React from 'react'

const Iframe = ({src}) => (
  <div className="prism-iframe">
    <iframe
      className="prism-iframe__iframe"
      frameBorder="0"
      width="100%"
      height="100%"
      src={src}/>

    <div className="prism-iframe__handle"/>
  </div>
)

export default Iframe
