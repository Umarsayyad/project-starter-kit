import React, {Component} from 'react';
import icons from '../styles/prs-styles/fonts/PRS-Icon-Font/selection.json'
import {white} from 'ansi-colors';
import Search from './Search.js';

class PrismIcons extends Component {

    render() {
        const getPath = (icon) => {
            return icon
                .icon
                .paths
                .join(' ');
        }

        return (

            <div className='flex-group'>
                {icons
                    .icons
                    .sort((a, b) => a.properties.name.localeCompare(b.properties.name))
                    .map((icon, index) => {
                        console.log(icon.properties.name);
                        console.log(icons.icons.length)
                        let {name, code} = icon.properties;
                        return (
                            <div key={index} className='icon-tile'>
                                <div className='card'>
                                    <span className={`prism-icon__demo icon prs-icon-${name}`}></span>

                                </div>
                                <p className='font-weight-bold'>
                                    {name}
                                </p>
                                <p
                                    style={{
                                    color: '#babcbe'
                                }}>
                                    .{name}
                                </p>
                            </div>
                        )
                    })
}

            </div>

        );
    }
}

export default PrismIcons;
