import React, {Component} from 'react'
import {Link} from 'gatsby';
import Icon from './Icon.js';

export class PrimaryNavItem extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isNavOn: false,
      currentPath: ''
    }
    this.toggleNav = this
      .toggleNav
      .bind(this)
  }

  componentDidMount() {
    let urlArray = location
      .pathname
      .split("/")
      .filter(item => item === this.props.group)
    if (urlArray.length > 0) {
      console.log(urlArray)
      this.setState({isNavOn: true})
    }
  }

  toggleNav(e) {
    if (this.props.subNavItem) {
      e.preventDefault();
      this.setState(prevState => ({
        isNavOn: !prevState.isNavOn
      }))
    } else {
      this.setState({isNavOn: true})
    }
  }
  render() {
    return (
      <li
        className={this.state.isNavOn
        ? 'prism-primary-nav__item is-active'
        : 'prism-primary-nav__item'}
        key={this.props.index}>
        <Link
          className={this.state.subNavItem
          ? 'prism-primary-nav__link prism-primary-nav__link--has-children'
          : 'prism-primary-nav__link'}
          to={this.props.href}
          onClick={this.toggleNav}>
          <span className="prism-primary-nav__text">{this.props.label}</span>
          {this.props.subNavItem && (<Icon styleModifier='prism-icon prism-primary-nav__icon' name='chevron-down'/>)}
        </Link>
        {this.props.subNavItem && this.props.subNavItem}
      </li>
    )
  }
}

export default PrimaryNavItem
