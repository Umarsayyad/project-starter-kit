import React from 'react'
import Prism from 'prismjs'
import {Link} from 'gatsby'

class Tabs extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      currentTab: ''
    }
    this
      .setCurrentTab
      .bind(this)
  }
  componentDidMount() {
    let urlArray = location
      .pathname
      .split("/");
    let urlEnd = urlArray[urlArray.length - 1]
    console.log('last' + urlEnd.length)

    if (urlEnd.length < 1) {
      let initialTab = this
        .props
        .tabs[0]
        .toLowerCase();
      this.setState({currentTab: initialTab})
    } else {
      this.setState({
        currentTab: location
          .pathname
          .split("/")
          .pop()

      })
    }

  }

  setCurrentTab(tab) {
    this.setState({currentTab: tab})
  }

  render() {
    return (
      <div className={'prism-tabs ' + this.props.styleModifier}>
        <ul className="prism-tabs__list" role="tablist">
          {this
            .props
            .tabs
            .map((item, index) => {
              console.log(item)
              return (
                <li
                  className={'prism-tabs__list-item ' + (this.state.currentTab === item.toLowerCase()
                  ? 'is-active'
                  : 'not')}
                  key={'tab-' + index}>
                  <a className="prism-tabs__link" href={`${item.toLowerCase()}`}>
                    {item}
                  </a>
                </li>
              )
            })}
        </ul>

      </div>
    )
  }
}

export default Tabs
