import React from 'react'
import classnames from 'classnames'

const Badge = ({status}) => {
  let badgeClass = classnames({
    'prism-badge': true,
    'prism-badge--positive': status === 'Complete',
    'prism-badge--negative': status === 'Deprecated' || 'Not Started',
    'prism-badge--caution': status === 'In Progress'
  })

  if (status) {
    return <span className={badgeClass}>{status}</span>
  }
}

export default Badge
