import React from 'react'
import Helmet from 'react-helmet'
import {StaticQuery, graphql} from 'gatsby'
import Header from './Header'

import '../styles/style.scss'
import '../styles/style-guide.css'

export default({children}) => {
  return (
    <StaticQuery
      query={graphql ` query LayoutQuery { allMarkdownRemark( sort: { order: ASC, fields: [frontmatter___title] } ) { edges { node { excerpt(pruneLength: 250) id frontmatter { title description status path group subgroup layout } } } } } `}
      render={({
      allMarkdownRemark: {
        edges: posts
      }
    }) => (
      <div className="prism-wrapper">
        <Helmet>
          <title>Umar Design System</title>
          <meta charSet="utf-8"/>
          <meta name="description" content="A design guide for Cox Automotive"/>
          <meta name="keywords" content="Manheim Design System"/>
          <link rel="icon" href="favicon.ico"/>
          <link rel="stylesheet" href="https://use.typekit.net/agm6qos.css"/>
        </Helmet>
        <div className="l-page-layout l-page-layout--two-column-fixed">
          <div className="l-page-layout__secondary">
            <Header
              siteTitle="Manheim Design Sytem"
              styleModifier="prism-header--vertical"
              navData={posts}/>
          </div>
          <div className="l-page-layout__main">
            <main role="main">{children}</main>
          </div>
        </div>
      </div>
    )}/>
  )
}
