import React from 'react';
import PropTypes from 'prop-types';
import * as DsComponents from '../../../ds-components/';
import ComponentDemoMd from './ComponentDemoMd';
import * as Examples from '../index';
import reactElementToJSXString from 'react-element-to-jsx-string';

const components = {
    button: DsComponents.Button
}

const sanitize = (_props) => {
    const props = Object.assign({
        displayName: _props.component
    }, _props);
    delete props.component;
    delete props.variants;

    return props;
}

const ComponentLive = (props) => {
    const {component, children} = props;

    const CurrentComponent = components[component];
    const compName = CurrentComponent;

    const compCode = reactElementToJSXString(
        <CurrentComponent/>, {showFunctions: true});
    console.log('name' + compName)
    // const variants = props     .variants     .split(' '); const VarMap =
    // variants.map(variant => <CurrentComponent className={`${variant}`} {
    // ...(sanitize(props)) }/>) const ComponentProps = CurrentComponent.propTypes;
    return (<CurrentComponent name={component} code={compCode} { ...(sanitize(props)) }/>)
}

export default ComponentLive;