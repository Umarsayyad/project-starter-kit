import React, {Component} from 'react';
import icons from '../json/selection.json';

class Icon extends Component {

    render() {
        const getPath = (icon) => {
            console.log(icon);
            return icon
                .icon
                .paths
                .join(' ');
        }

        const {styleModifier, name} = this.props;
        console.log(name);
        return (

            <svg className={`${this.props.styleModifier}`} viewBox="0 0 1024 1024">
                {icons
                    .icons
                    .filter(icon => icon.properties.name === name)
                    .map((icon, index) => {
                        let {name, code} = icon.properties;
                        return (

                            <path key={index} d={getPath(icon)}></path>

                        )
                    })}
            </svg>
        )
    }
}

export default Icon;