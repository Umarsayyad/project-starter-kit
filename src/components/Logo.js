import React from 'react'
import {Link} from 'gatsby'
import umarlogo from '../images/umaricon.svg';

const Logo = () => (
  <h3 className="prism-logo">
    <Link className="prism-logo__link" to="/">
      <img className='prism-logo__img' src={umarlogo}/>
    </Link>
  </h3>
)

export default Logo
