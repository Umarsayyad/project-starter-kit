import React, { Component } from 'react'
import Accordion from '../../../ds-components/Accordion/Accordion.js'
import AccordionItem from '../../../ds-components/Accordion/AccordionItem.js'
import ComponentUsageDemo from '../DemoCards/ComponentUsageDemo.js'
import ItemFilter from '../../../ds-components/Accordion/ItemFilter.js'

class AccordionUsage extends Component {
  render() {
    return (
      <ComponentUsageDemo className="prism-accordion">
        <Accordion>
          <AccordionItem heading="Filter Name">
            <div className="accordion-item__body">
              <ItemFilter />
            </div>
          </AccordionItem>
          <AccordionItem heading="Filter Name">
            <div className="accordion-item__body">
              <ItemFilter />
            </div>
          </AccordionItem>
          <AccordionItem heading="Filter Name">
            <div className="accordion-item__body">
              <ItemFilter />
            </div>
          </AccordionItem>
        </Accordion>
      </ComponentUsageDemo>
    )
  }
}

export default AccordionUsage
