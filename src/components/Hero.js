import React from 'react'
import markdownIt from 'markdown-it'

const Hero = ({title, description, meta, backgroundImageUrl, footer}) => {
  const md = markdownIt({html: true, linkify: true})

  return (
    <div
      className="prism-hero"
      style={{
      backgroundImage: 'url(' + backgroundImageUrl + ')'
    }}>
      <div className="prism-hero__body">
        <h1 className="prism-hero__title">{title}</h1>
        {description && (<div
          className="prism-hero__desc"
          dangerouslySetInnerHTML={{
          __html: md.render(description)
        }}/>)}
        {meta && <div className="prism-hero__meta">{meta}</div>}
        {footer && <div className="prism-hero__footer">{footer}</div>}
      </div>
    </div>
  )
}

export default Hero
