---
path: /elements/input/usage
layout: custom
title: Text Inputs
description: Input
group: elements
status: Complete
tabs: ['Usage', 'Code', 'Style']
---

# Usage

---

### Definition

The input allows a user to type and submit alpha-numeric text. Variations and rules for specific input types are specified below.

Full-width fields are only allowed on the XS grid (0-479). In all other cases, inputs must span to a specified number of columns in the layout.

### Common Characteristics

Text inputs are normally 40px high, but a small 32px alternative may be used. Whichever size is used, use the same size for other form elements like selects and buttons.

When using text inputs always declare the appropriate HTML input type to ensure that mobile browsers invoke the correct mobile keyboard.

All text inputs have these states: disabled, active, and focused.

<usage-demo><text-input></text-input></usage-demo>

---

### Label Options

In some scenarios it may be desired to move or hide the input label. Right aligned labels can save vertical space in the UI. Hidden labels should be used with caution, and only when there are very few fields (e.g. a login form), or where the input required is very well understood (e.g. the user’s street address).

<usage-demo><form-inline></form-inline></usage-demo>

---

### Required vs Optional

When designing forms, aim to include only required fields. Avoid optional fields unless designed a search/filter scenario, where all fields may be optional.

Do not mark required form fields with an asterisk (\*). Instead, mark optional fields as “optional” next to the label when they are an exception that can't be avoided. Optionally, designers can include a text instruction, “All fields are required.” at the top of the form.

<usage-demo><form-optional></form-optional></usage-demo>

---

### Validation

When an input is required, validation should take place as soon as technically possible in the user's experience, beginning with while a field is in focus (e.g. a matching password field), then when a field loses focus (e.g. an email field), etc.

<usage-demo><form-validation></form-input></usage-demo>

---

### Types and Variants

<usage-demo><input-icon></input-icon></usage-demo>

---
