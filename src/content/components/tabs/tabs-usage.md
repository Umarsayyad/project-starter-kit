---
layout: custom
group: components
path: /components/tabs/usage
title: Tabs
component: Tabs
heading: Tabs
description: Default Tabs for Components
tabs: [Usage, Code, Style]
---

**Accordions are used to manage large amounts of content. Information can be dynamically linked to selection criteria that is organized within a nested checklist that opens and closes at the user's discretion.**

#Demo

<component-demo><tab-example></tab-example></component-demo>

#Use Cases

Accordions are used primarily in our Search Results Page along the left-hand side. Nested elements such as forms, and drop down menus can be nested in seperate stacks.

---

####Nested Elements

To add further functionality other nested elements can be added as list items that the user can interact with. As is the case with the search result page from OVE.com.

</div>
