---
layout: custom
group: components
path: /components/accordion/code
title: Accordion 
component: accordion
heading: Accordion
description: An accordion provides an expandable details-summary view.
tabs: [Usage, Code, Style]
usage:
- title: Accordion
  description: An accordion provides an expandable details-summary view. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet. Sed sit amet arcu aliquet, molestie justo at, auctor nunc. Phasellus ligula ipsum, volutpat eget semper id, viverra eget nibh.
  demo: accordion
- title: Accordion
  description: This is a text passage to describe some of the usage for this pattern.
  
---

#Demo
 
<div class='component-demo-body'>
<prism-accordion></prism-accordion>

#<span class='markdown-section-heading'> Documentation</span>

####SCSS

 
<md-table>
| ClassName                   | Description                                      |
| --------------------------- | ------------------------------------------------ |
| `.prism-accordion__item`    | Selects the container of each expandable element |
| `.prism-accordion__arrow`   | Arrow favicon up & down                          |
| `.prism-accordion__title`   | Customize the heading of each collapse item      |
| `.prism-accordion__content` | 'Accordion element body content'                 |
</md-table>

 

####JavaScript
Access these modifiers with `options`

<md-table>
| Modifier        | Type     |
| --------------- | -------- |
| `Disabled `     | `bool`   |
| `Items`         | `num`    |
| `Title`         | `string` |
| `StyleModifier` | `string` |
</md-table>


####Options
Access these modifiers with `options`

<md-table>
| ClassName                       | Property                |
| ------------------------------- | ----------------------- |
| .prism-accordion__item.isActive | Selects the active item |
| .prism-accordion__item.isClosed | Selects the closed itm  |

</md-table>
</div>
