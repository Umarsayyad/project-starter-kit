---
path: /elements/icons/usage
layout: foundation
title: Icons
description: Icons
tabs: [Library, Usage]
---

#Sizing

The base size for Manheim Fonts are 16 but can vary depending on the responsive layout they are displayed in. Use the following sizes that fit the need of your design:

---

 
| **12px**                                                            | **16px**                                                             | **24px**                                                            |
| ------------------------------------------------------------------- | -------------------------------------------------------------------- | ------------------------------------------------------------------- |
| <span class='prism-icon__demo icon prs-icon-envelop5 fs-12'></span> | <span class='prism-icon__demo icon prs-icon-envelop5  fs-16'></span> | <span class='prism-icon__demo icon prs-icon-envelop5 fs-24'></span> |
 
 
