import React, {Children, Component} from 'react';
import CollapseItem from '../Accordion/CollapseExample';
import Markdown from 'markdown-to-jsx';
import classnames from 'classnames';
import {PrismCode} from 'react-prism';
import reactElementToJSXString from 'react-element-to-jsx-string';
import ReactDOMServer from 'react-dom/server';
import reactToString from 'react-to-string';
import Prism from 'prismjs/components/prism-core';
import jsxToString from 'jsx-to-string';
import {LiveProvider, LiveEditor, LiveError, LivePreview} from 'react-live'
import {
    Card,
    CardBody,
    CardTitle,
    CardText,
    CardHeader,
    CardFooter,
    Button,
    TabContent,
    TabPane,
    Nav,
    NavItem,
    NavLink,
    Row,
    Col
} from 'reactstrap';
import LiveCode from '../../components/LiveCode';

class CodeTabs extends Component {
    constructor(props) {
        super(props);

        this.toggle = this
            .toggle
            .bind(this);
        this.state = {
            activeTab: '1'
        };
    }

    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({activeTab: tab});
        }
    }
    render() {

        return (

            <CardBody className='demo-card-body'>
                <Nav tabs>
                    <NavItem>
                        <NavLink
                            id='leftTab'
                            className={classnames({
                            active: this.state.activeTab === '1'
                        })}
                            onClick={() => {
                            this.toggle('1');
                        }}>
                            React
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink
                            className={classnames({
                            active: this.state.activeTab === '2'
                        })}
                            onClick={() => {
                            this.toggle('2');
                        }}>
                            HTML
                        </NavLink>
                    </NavItem>
                </Nav>
                <TabContent activeTab={this.state.activeTab}>
                    <TabPane tabId="1">

                        {this.props.children}
                    </TabPane>
                    <TabPane tabId="2">

                        {this.props.children}

                    </TabPane>
                </TabContent>
            </CardBody>

        );
    }
}

export default CodeTabs;