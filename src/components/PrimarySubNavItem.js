import React, {Component} from 'react'
import {Link, navigate} from 'gatsby'
import Icon from './Icon.js';

export class PrimarySubNavItem extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isSubNavOn: true,
      currentPath: false
    }
    this.toggleSubNav = this
      .toggleSubNav
      .bind(this)
  }
  componentDidMount() {
    let currentUrl = location
      .pathname
      .split("/")
      .filter(item => item === this.props.title.toLowerCase())
    if (currentUrl.length > 0) {
      this.setState({currentPath: true})
    }
  }
  toggleSubNav(e) {
    if (this.props.subSubNavItems) {
      e.preventDefault()
      this.setState(prevState => ({
        isSubNavOn: !prevState.isSubNavOn
      }))
    } else {
      this.setState({isSubNavOn: true})
    }
  }

  render() {

    return (
      <li
        className={this.state.isSubNavOn
        ? 'prism-primary-nav__subitem '
        : 'prism-primary-nav__subitem is-active'}
        key={this.props.url}>
        <Link
          className={this.state.currentPath
          ? 'prism-primary-nav__sublink is-active'
          : 'prism-primary-nav__sublink '}
          to={this.props.url}
          onClick={this.toggleSubNav}>
          <span className="prism-primary-nav__text">{this.props.title}</span>
          {this.props.subSubNavItems && (<Icon
            styleModifier='prism-icon prism-primary-nav__subicon'
            name='chevron-down'/>)}
        </Link>
        {this.props.subSubNavItems && this.props.subSubNavItems}
      </li>
    )
  }
}

export default PrimarySubNavItem
