# Umar's project jump starter

Starting a new project? Need some where to refer to? Well use the is Project guide to help you jump start your project.
Everything within in here is React and Gatsby compatible. Even if you're using a diffrent interface, every thing within
this project is still compatible with Redux or contentful.

## The bread and butter

Umar's project jump starter is built using [Gatsby](https://www.gatsbyjs.org/), a site generator built with [React](https://reactjs.org/).

### GatsbyNode.js

- Defines GraphQL Theory
- Links templates with markdown data with createPage.
- Pulls markdown content file and React components, renders into React.
- Follow link to understand how gatsbyNode.js effects
the project https://www.gatsbyjs.org/docs/node-apis/

### Content

Mardown files that become the actual pages

- Foundation/
- Elements/
- Components/
- Patterns/
- page-templates.md

### DS-Components/

- React Components that make up the Design System

### Gatsby Configuration Plugins
- gatsby-transformer-remark
- gatsby-remark-images
- gatsby-remark-prismjs
- gatsby-remark-component
- gatsby-transformer-json
- gatsby-plugin-sass
- gatsby-plugin-sharp
- gatsby-plugin-react-helmet

### 



## Getting Started

1. Download or clone the files 
2. In the command line, navigate to the root of the project and run the `yarn or Gatsby` command. This will download the project dependencies
3. Run `yarn start` to run the project.


