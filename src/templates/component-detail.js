import React, {Component} from 'react'
import {graphql} from 'gatsby'
import Helmet from 'react-helmet'
import reactElementToJSXString from 'react-element-to-jsx-string'
import ReactDOMServer from 'react-dom/server'
import classnames from 'classnames';

import {FaChevronLeft, FaChevronRight} from 'react-icons/fa'
import Layout from '../components/layout'
import Link from '../components/Link'
import Tags from '../components/Tags'
import ComponentDetailHeader from '../components/ComponentDetailHeader'
import Tab from '../components/Tab'
import Tabs from '../components/Tabs'
import Section from '../components/Section'
import Well from '../components/Well'
import Table from '../components/Table'
import ContentBlock from '../components/ContentBlock'
import ButtonLink from '../components/ButtonLink'
import Button from '../components/Button'
import ComponentExample from '../components/ComponentExample'
import markdownIt from 'markdown-it'
import ColorCard from '../components/ColorCard'
import rehypeReact from 'rehype-react';

const renderAst = new rehypeReact({
  createElement: React.createElement,
  components: {
    "color-card": ColorCard
  }
}).Compiler

export class ComponentDetail extends Component {
  componentDidMount() {
    const script = document.createElement('script')
    script.src = '../../vendor/clipboard.min.js'
    document
      .body
      .appendChild(script)

    const scriptCopy = document.createElement('script')
    scriptCopy.src = '../../vendor/copy.js'
    document
      .body
      .appendChild(scriptCopy)
  }

  render() {

    const md = markdownIt({html: true, linkify: true})
    const {markdownRemark: post} = this.props.data
    const {next, prev} = this.props.pageContext

    return (

      <Layout>
        <ComponentDetailHeader
          title={post.frontmatter.title}
          description={post.frontmatter.description}
          status={post.frontmatter.status}/>
        <div className="l-container">
          <Helmet title={`${post.frontmatter.title}`}/>

          <div className="prism-text-passage">
            <h3>Overview</h3>
            <p>{post.frontmatter.description}</p>

          </div>

          {(post.frontmatter.variations && post.frontmatter.variations.length) === 1 && post.frontmatter.variations.map((item, index) => (
            <div key={index}>
              <Well>
                <ComponentExample component={item.component}/>
              </Well>

            </div>
          ))}

          {(post.frontmatter.usage || post.frontmatter.use || post.frontmatter.altUse) && (
            <Section title="Usage">
              <div className="prism-text-passage">
                {post.frontmatter.usage && post
                  .frontmatter
                  .usage
                  .map((item, index) => (
                    <div key={index}>
                      <h3>{item.title}</h3>
                      <div
                        dangerouslySetInnerHTML={{
                        __html: md.render(item.description)
                      }}/>
                    </div>
                  ))}
              </div>
              <ul className="l-grid">
                <li className="l-grid__item">
                  {post.frontmatter.use && post
                    .frontmatter
                    .use
                    .map((item, index) => (<ContentBlock
                      key={index}
                      title={item.title}
                      description={item.description}
                      styleModifier="prism-content-block--success"/>))}
                </li>
                <li className="l-grid__item">
                  {post.frontmatter.altUse && post
                    .frontmatter
                    .altUse
                    .map((item, index) => (<ContentBlock
                      key={index}
                      title={item.title}
                      description={item.description}
                      styleModifier="prism-content-block--error"/>))}
                </li>
              </ul>
            </Section>
          )}

          {post.frontmatter.classes && (
            <Table
              tableHeaderColumns={['Class Name', 'Description']}
              tbody={post
              .frontmatter
              .classes
              .map((item, index) => (
                <tr className="prism-table__row" key={index}>
                  <td className="prism-table__cell">
                    <code>{item.className}</code>
                  </td>
                  <td
                    className="prism-table__cell"
                    dangerouslySetInnerHTML={{
                    __html: md.render(item.description)
                  }}/>
                </tr>
              ))}/>

          )}

          <div className="flex-row">
            <div className="half-width">

              {post.frontmatter.primaryColors && (
                <Table
                  tableHeaderColumns={['Primary', '']}
                  tbody={post
                  .frontmatter
                  .primaryColors
                  .map((item, index) => (
                    <tr className="prism-table__row" key={index}>
                      <td className="prism-table__cell">
                        <p>{item.color}</p>
                        <p>{item.hex}</p>
                      </td>
                      <td
                        style={{
                        backgroundColor: `#${item.hex}`
                      }}
                        className="prism-table__color"/>
                    </tr>
                  ))}/>

              )}
            </div>
            <div className="half-width">
              {post.frontmatter.secondaryColors && (
                <Table
                  className="color-table"
                  tableHeaderColumns={['Secondary', '']}
                  tbody={post
                  .frontmatter
                  .secondaryColors
                  .map((item, index) => (
                    <tr className="prism-table__row" key={index}>
                      <td className="prism-table__cell">
                        <p>{item.color}</p>
                        <p>{item.hex}</p>
                      </td>
                      <td
                        style={{
                        backgroundColor: `#${item.hex}`
                      }}
                        className="prism-table__color"/>
                    </tr>
                  ))}/>
              )}
            </div>

          </div>

          <div
            className="prism-text-passage"
            dangerouslySetInnerHTML={{
            __html: post.html
          }}/>

          <Tags list={post.frontmatter.tags || []}/>

          <div className="prism-pagination">
            {prev && (
              <Link className="prism-pagination__link" to={prev.frontmatter.path}>
                <FaChevronLeft/> {prev.frontmatter.title}
              </Link>
            )}
            {next && (
              <Link className="prism-pagination__link" to={next.frontmatter.path}>
                {next.frontmatter.title}
                <FaChevronRight/>
              </Link>
            )}
          </div>

          <div>
            {renderAst(post.htmlAst)}</div>

          {post.frontmatter.finePrint && (
            <Section title="Fine print">
              {post
                .frontmatter
                .finePrint
                .map((item, index) => (
                  <div className="prism-text-passage" key={index}>
                    <ul>
                      <li>Version: {item.version}</li>
                      <li>Last Updated: {item.update}</li>
                      <li>Owner: {item.owner}</li>
                    </ul>
                  </div>
                ))}
              <ButtonLink href="#" text="Discuss this component"/>
            </Section>
          )}
        </div>
      </Layout>
    )
  }
}

export default ComponentDetail

export const pageQuery = graphql `
  query BlogPostQuery($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      htmlAst
      frontmatter {
        path
        tags
        title
        description
        status
        group
        subgroup
        variations {
          title
          description
          styleModifier
          component
        }
        usage {
          description
        }
        use {
          title
          description
        }
        altUse {
          title
          description
        }
        classes {
          className
          required
          description
          modifier
          recommended
        }
        finePrint {
          version
          update
          owner
        }
      }
    }
  }
`
