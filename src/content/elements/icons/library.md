---
path: /elements/icons/library
layout: foundation
title: Icons
description: Icons
tabs: [Library, Usage]
---

**Icons help commuicate key touch-points in the experience. They are to be used in comination with text and without text.**

# Manheim Font

The Manheim font leverages ico-moon's font library and is a css-managed web font. There are over 2000 fonts in total of which many were handpicked to be used across experiences. To use the font in your designs download the fonts to your font manager to preview and use them.

## <button class='prism-btn '>DOWNLOAD FONTS</button>

---

<prism-icons></prism-icons>
