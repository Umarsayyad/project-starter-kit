import React, {Component} from 'react';
import {Collapse, Button, CardBody, Card} from 'reactstrap';

class CollapseExample extends Component {
    constructor(props) {
        super(props);
        this.toggle = this
            .toggle
            .bind(this);
        this.state = {
            collapse: false
        };
    }

    toggle() {
        this.setState({
            collapse: !this.state.collapse
        });
    }

    render() {
        return (
            <div className='prism-accordion-item'>
                <Button
                    className="prism-accordion-item__btn"
                    color="white"
                    onClick={this.toggle}
                    style={{
                    textAlign: 'left',
                    padding: '1rem',
                    border: '1px solid rgba(0,0,0,.125)',
                    borderRadius: '0',
                    borderLeft: '0',
                    borderRight: '0',
                    fontSize: '1.6rem'
                }}
                    block>Toggle</Button>
                <Collapse isOpen={this.state.collapse}>
                    <Card
                        style={{
                        borderRadius: '0',
                        border: 'none'
                    }}>
                        <CardBody>
                            <p>
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes
                                anderson cred nesciunt sapiente ea proident.</p>
                        </CardBody>
                    </Card>
                </Collapse>

            </div>
        );
    }
}

export default CollapseExample;
