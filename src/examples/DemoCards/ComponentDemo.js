import React, {Children, Component} from 'react';
import Markdown from 'markdown-to-jsx';
import classnames from 'classnames';
import {PrismCode} from 'react-prism';
import reactElementToJSXString from 'react-element-to-jsx-string';
import ReactDOMServer from 'react-dom/server';
import reactToString from 'react-to-string';
import Prism from 'prismjs/components/prism-core';
import jsxToString from 'jsx-to-string';
import {LiveProvider, LiveEditor, LiveError, LivePreview} from 'react-live'
import {
    Card,
    CardBody,
    CardTitle,
    CardText,
    CardHeader,
    CardFooter,
    Button,
    TabContent,
    TabPane,
    Nav,
    NavItem,
    NavLink,
    Row,
    Col
} from 'reactstrap';

class ComponentDemo extends Component {
    constructor(props) {
        super(props);

        this.toggle = this
            .toggle
            .bind(this);
        this.state = {
            activeTab: '1'
        };
    }

    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({activeTab: tab});
        }
    }

    componentDidMount() {}
    render() {

        return (
            <div className='demo prism-section'>
                <Card>

                    <CardHeader className='component-demo__box'>
                        {this.props.children}
                    </CardHeader>
                    <CardBody className='demo-card-body'>
                        <Nav tabs>
                            <NavItem>
                                <NavLink
                                    id='leftTab'
                                    className={classnames({
                                    active: this.state.activeTab === '1'
                                })}
                                    onClick={() => {
                                    this.toggle('1');
                                }}>
                                    React
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    className={classnames({
                                    active: this.state.activeTab === '2'
                                })}
                                    onClick={() => {
                                    this.toggle('2');
                                }}>
                                    HTML
                                </NavLink>
                            </NavItem>
                        </Nav>
                        <TabContent className='component-demo__code' activeTab={this.state.activeTab}>
                            <TabPane tabId="1">

                                <PrismCode component='pre' className="language-jsx">
                                    {reactElementToJSXString(this.props.children)}
                                </PrismCode>

                            </TabPane>
                            <TabPane tabId="2">
                                <pre>
                                <code className="language-markup">
                                     
                                    {ReactDOMServer.renderToStaticMarkup(this.props.children)}

                                </code>
                                    </pre>
                            </TabPane>
                        </TabContent>
                    </CardBody>

                </Card>

                <hr/>

            </div>
        );
    }
}

export default ComponentDemo;