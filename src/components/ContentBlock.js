import React from 'react'
import classnames from 'classnames'
import markdownIt from 'markdown-it'

const ContentBlock = ({title, description, styleModifier}) => {
  let contentBlockClass = classnames({
    'prism-content-block': true,
    'prism-content-block--success': styleModifier === 'prism-content-block--success',
    'prism-content-block--error': styleModifier === 'prism-content-block--error'
  })

  const md = markdownIt({html: true, linkify: true})

  return (
    <div className={contentBlockClass}>
      <h4 className="prism-content-block__title">{title}</h4>
      <div
        className="prism-content-block__desc prism-text-passage"
        dangerouslySetInnerHTML={{
        __html: md.render(description)
      }}/>
    </div>
  )
}

export default ContentBlock
