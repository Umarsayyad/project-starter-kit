import React from 'react';
import {
    Form,
    FormGroup,
    Label,
    Input,
    FormFeedback,
    FormText
} from '../../../ds-components/';
import PassForm from '../../components/PassForm.js'
import TextAreaCounter from '../../components/TextAreaCounter.js'
import NumberInputGroup from '../../components/NumberInput';

export const TextInput = () => (
    <div className="input-demo__card card container">
        <form>
            <div className="form-row">
                <div className="col form-group">
                    <label htmlFor="formGroupExampleInput">Default
                    </label>
                    <input
                        type="text"
                        className="form-control"
                        id="formGroupExampleInput"
                        placeholder="Form Control Regular"/>
                </div>
            </div>
            <div className="form-row">
                <div className="col form-group">
                    <label htmlFor="formGroupExampleInput3">Small</label>
                    <input
                        type="text"
                        className="form-control-sm form-control "
                        id="formGroupExampleInput3"
                        placeholder="Form Contol Small"/>
                </div>
            </div>
        </form>
    </div>

)

export class FormValidation extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            valid1: null,
            valid2: null
        }
        this.handleSubmit = this
            .handleSubmit
            .bind(this);

    }

    handleSubmit() {
        this.setState({valid1: true});
        this.setState({valid2: true})
    }

    render() {
        const {valid1, valid2} = this.state;
        return (
            <div className="input-demo__card card container">
                <Form>
                    <FormGroup>
                        <Label for="exampleValid">Valid input</Label>

                        <Input
                            id='exampleValid'
                            type='text'
                            valid={valid1}
                            placeholder='User entry passed validation'/>
                        <FormFeedback className='mt-8' valid>
                            <span className='icon prs-icon-checkmark-circle form-feedback__icon'></span>It's all good message</FormFeedback>

                    </FormGroup>
                </Form>
                <Form>
                    <FormGroup>
                        <Label for="exampleInValid">Valid input</Label>

                        <Input
                            id='exampleInValid'
                            className='form-control-sm '
                            invalid={valid2}
                            placeholder='User entry failed validation'/>
                        <FormFeedback className='mt-8' invalid>
                            <span className='icon prs-icon-notification-circle form-feedback__icon  '></span>There's a problem message</FormFeedback>
                    </FormGroup>

                </Form>
                <form-row>
                    <span onClick={this.handleSubmit} className='col-3 btn btn-primary'>Submit to validate</span>
                </form-row>

            </div>
        );
    }
}

export class FormOptional extends React.Component {
    render() {
        return (
            <div className='container'>

                <div className="input-demo__card card container mb-3">
                    <Form>

                        <FormGroup>
                            <div className='d-flex justify-content-between'>
                                <Label for="exampleEmail">Default</Label>

                                <small id="optionalLabel" className="form-text text-muted flex-end">Optional</small>
                            </div>

                            <Input optional type='text' placeholder='Form with optional input'/>

                        </FormGroup>

                    </Form>
                </div>

                <div className="input-demo__card card container">
                    <Form>
                        <div className="col">
                            <small className='display-block text-right'>All fields are required</small>

                        </div>
                        <div className="form-row">
                            <div className="col form-group">
                                <label htmlFor="formGroupExampleInput">Default
                                </label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="formGroupExampleInput"
                                    placeholder="Form Control Regular"/>
                            </div>
                        </div>
                        <div className="form-row">
                            <div className="col form-group">
                                <label htmlFor="formGroupExampleInput3">Small</label>
                                <input
                                    type="text"
                                    className="form-control-sm form-control "
                                    id="formGroupExampleInput3"
                                    placeholder="Form Contol Small"/>
                            </div>
                        </div>
                    </Form>
                </div>
            </div>
        );
    }
}

export class FormInlineInput extends React.Component {
    render() {
        return (
            <div className="input-demo__card card container">
                <Form >
                    <div className="form-row align-items-center pb-3">
                        <div className="col-xs-12 col-sm-1 text-xs-left text-sm-right">
                            <Label for="exampleEmail">Inline
                            </Label>
                        </div>
                        <div className="col-10">
                            <Input type="text" placeholder='Input with inline label'/>
                        </div>
                    </div>
                    <div className="form-row align-items-center">

                        <div className="col-xs-12 col-sm-1 text-xs-left text-sm-right">
                            <Label className="sr-only pr-2 flex-nowrap" for="exampleEmail">Label</Label>
                        </div>
                        <div className="col-sm-10 col-xs-12">
                            <Input type="text" placeholder="Label Screen Reader Only"/>
                        </div>

                    </div>

                </Form>
            </div>
        );
    }
}

export class TextInputIcon extends React.Component {
    render() {
        return (
            <div className="container p-0">

                <div className="input-demo__card card container mb-2">
                    <form>
                        <div className="form-row">
                            <div className="col form-group">

                                <label className="control-label">Input with icon left</label>
                                <div className="d-flex align-items-center inline">

                                    <i className="icon text-input__icon-left prs-icon-user"/>

                                    <input
                                        type="text"
                                        className="form-control text-input__has-icon-left"
                                        placeholder="Username"/>

                                </div>

                            </div>

                        </div>
                    </form>
                </div>
                <div className="input-demo__card card container mb-2">
                    <form>
                        <div className="form-row">
                            <div className="col form-group pt-2">

                                <label className="control-label">Input with icon right</label>
                                <div className="d-flex align-items-center inline">
                                    <input
                                        type="text"
                                        className="form-control text-input__has-icon-right"
                                        placeholder="Search"/>
                                    <button className='btn p-0'><i className="icon text-input__icon-right prs-icon-search"/></button>
                                </div>

                            </div>
                            <div className="col form-group pt-2">

                                <label className="control-label">Input with icon button right</label>
                                <div className="input-group">
                                    <input
                                        type="text"
                                        className="form-control form-control-has-append__right"
                                        placeholder="Search"/>
                                    <div className='input-group-append'>
                                        <span className="input-group-text btn  btn-secondary">
                                            <i className="icon prs-icon-search"/>
                                        </span>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </form>
                </div>
                <div className="input-demo__card card container mb-2">
                    <form>

                        <div className="form-group">
                            <div className='d-flex justify-content-between'>
                                <Label for="exampleInputPassword1">Password</Label>

                            </div>

                            <PassForm/>
                        </div>

                    </form>
                </div>
                <div className="input-demo__card card container mb-2">
                    <form>
                        <div className="form-group">
                            <TextAreaCounter label='Text Area' maxLength={10} rows='4'/>
                        </div>

                    </form>
                </div>
                <NumberInput/>
            </div>
        );
    }
}

export class NumberInput extends React.Component {
    render() {
        return (
            <div className="input-demo__card card container">
                <form className='mx-auto'>
                    <NumberInputGroup name='Number Input' number={50}/>
                </form>
            </div>
        );
    }
}
