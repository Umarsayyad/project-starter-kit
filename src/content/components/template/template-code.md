---
layout: custom
group: components
path: /components/template/code
title: Template
component: Template
heading: Template
description: Default Template for Components
tabs: [Usage, Code, Style]
---


#Demo
 
<div class='component-demo-body'>
<prism-accordion></prism-accordion>

#<span class='markdown-section-heading'> Documentation</span>

####SCSS

 
<md-table>
| ClassName                   | Description                                      |
| --------------------------- | ------------------------------------------------ |
| `.prism-accordion__item`    | Selects the container of each expandable element |
| `.prism-accordion__arrow`   | Arrow favicon up & down                          |
| `.prism-accordion__title`   | Customize the heading of each collapse item      |
| `.prism-accordion__content` | 'Accordion element body content'                 |
</md-table>

 

####JavaScript
Access these modifiers with `options`

<md-table>
| Modifier        | Type     |
| --------------- | -------- |
| `Disabled `     | `bool`   |
| `Items`         | `num`    |
| `Title`         | `string` |
| `StyleModifier` | `string` |
</md-table>


####Options
Access these modifiers with `options`

<md-table>
| ClassName                       | Property                |
| ------------------------------- | ----------------------- |
| .prism-accordion__item.isActive | Selects the active item |
| .prism-accordion__item.isClosed | Selects the closed itm  |

</md-table>
</div>
