import React from 'react'
import Button from '../Button'

const ButtonExampleSmall = () => {
  return (
    <div>
      <Button text="Button Text" styleModifier="prism-btn--small" />
    </div>
  )
}

export default ButtonExampleSmall
