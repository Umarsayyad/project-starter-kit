---
layout: custom
group: components
path: /components/accordion/style
title: Accordion 
component: Accordion
heading: Accordion
description: An accordion provides an expandable details-summary view.
tabs: [Usage, Code, Style]
---
<md-table>
# Color
| Class                                | Property   | SCSS             | HEX     |
| ------------------------------------ | ---------- | ---------------- | ------- |
| `.prism-- accordion__title `         | color      | $text-01         | #152935 |
| `.prism-- accordion__content `       | color      | $text-01         | #152935 |
| `.prism-- accordion__arrow `         | fill       | $ui-05           | #5a6872 |
| `.prism-- accordion__arrow : hover ` | fill       | $hover-secondary | #3d70b2 |
| `.prism-- accordion__item `          | border-top | $ui-04           | #8897a2 |
</md-table>
--- 


# Typography
<md-table>
| Class                          | Font-size (px/rem) | Font-weight  | Text style |
| ------------------------------ | ------------------ | ------------ | ---------- |
| `.prism-- accordion__title `   | 16 / 1             | Normal / 400 | p          |
| `.prism-- accordion__content ` | 14 / 0.875         | Normal / 400 | -          |
</md-table>
--- 

# Structure

There is no limit to the height of an open row, however, the padding specs below should be followed. The width of an Accordion varies based on the content, layout, and page design. The chevron icon can be found on the iconography page. 

<md-table>
| ClassName                           | Property                   | px/rem     | Spacing token |
| ----------------------------------- | -------------------------- | ---------- | ------------- |
| `.prism--accordion__heading `       | height                     | 40 / 2.5   | -             |
| `.prism--accordion__arrow `         | width                      | 10 / 0.635 | -             |
| `.prism--accordion__item `          | border-top                 | 1          | -             |
| `.prism--accordion__title `         | margin-left                | 16 / 1     | $spacing-md   |
| `.prism--accordion__content `       | padding-right, padding-top | 16 / 1     | $spacing-md   |
| `.prism--accordion__content `       | padding-left               | 40 / 2.5   | $spacing-2xl  |
| `.prism--accordion__item-- active ` | padding-bottom             | 24 / 1.5   | $spacing-lg   |
</md-table>