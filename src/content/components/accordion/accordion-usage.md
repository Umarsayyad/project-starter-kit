---
layout: custom
group: components
path: /components/accordion/usage
title: Accordion
component: Accordion
heading: Accordion
role: main
description: An accordion provides an expandable details-summary view.
tabs: [Usage, Code, Style]
---

**Accordions are used to manage large amounts of content. Information can be dynamically linked to selection criteria that is organized within a nested checklist that opens and closes at the user's discretion.**

#Demo

<accordion-usage></accordion-usage>

#Use Cases

Accordions are used primarily in our Search Results Page along the left-hand side. Nested elements such as forms, and drop down menus can be nested in seperate stacks.

---

####Nested Elements

To add further functionality other nested elements can be added as list items that the user can interact with. As is the case with the search result page from OVE.com.

<div class='image-wrap'>
<img class='prism-demo-image' src='../../images/accordion-artifact-1-1.png'>
</img>

<img-caption>
In the example above, a dynamic header and a search form is nested with the accordion.
</img-caption>
</div>
