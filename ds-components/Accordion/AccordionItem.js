import React, { Component } from 'react'
import { Collapse, Button, CardBody, Card } from 'reactstrap'


class AccordionItem extends Component {
  constructor(props) {
    super(props)
    this.toggle = this.toggle.bind(this)
    this.state = {
      collapse: false,
      icon: 'btn-icon__right icon prs-icon-chevron-down',
    }
  }

  toggle() {
    this.setState({
      collapse: !this.state.collapse,
    })
    if (this.state.collapse) {
      this.setState({ icon: 'btn-icon__right icon prs-icon-chevron-down' })
    } else {
      this.setState({ icon: 'btn-icon__right icon prs-icon-chevron-up' })
    }
  }

  render() {
    const heading = this.props.heading

    return (
      <li className="prism-accordion-item">
        <Button
          className="prism-accordion-item__btn"
          color="white"
          onClick={this.toggle}
          style={{
            textAlign: 'left',
            padding: '1rem',
            border: '1px solid rgba(0,0,0,.125)',
            borderRadius: '0',
            borderLeft: '0',
            borderRight: '0',
            fontSize: '1.6rem',
            display: 'flex',
            justifyContent: 'space-between',
          }}
          block
        >
          <span className="accordion-item-heading">{heading}</span>
          <span>
            <i className = 'btn-icon__right icon prs-icon-chevron-down'></i>
          </span>
        </Button>
        <Collapse isOpen={this.state.collapse}>
          <Card>
            <CardBody className="bg-ash prism-accordion-content">
              {this.props.children}
            </CardBody>
          </Card>
        </Collapse>
      </li>
    )
  }
}

export default AccordionItem
