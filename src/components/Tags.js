import React from 'react'
import {Link} from 'gatsby'

export default function Tags({
  list = []
}) {
  return (
    <ul className="prism-tag-list">
      {list.map(tag => (
        <li className="prism-tag-list__item" key={tag}>
          <Link to={`/tags/${tag}`}>{tag}</Link>
        </li>
      ))}
    </ul>
  )
}
