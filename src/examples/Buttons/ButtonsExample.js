import React, {Component} from 'react';
import Button from '../../../ds-components/Button/Button.js';
import PropTypes from 'prop-types';
import {ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem} from '../../../ds-components/'

export class ButtonsCommon extends Component {
    render() {
        const buttonProps = Button.propTypes
        console.log(buttonProps)
        return (
            <div className="container">
                <div className="row d-inline-flex align-items-center ">
                    <button
                        type="button"
                        color="secondary"
                        className="m-8 btn d-inline-flex btn-secondary">
                        CONTAINED
                    </button>
                    <button type="button" className="m-8 btn btn-outline-primary ">
                        OUTLINED
                    </button>
                    <button type="button" className="btn btn-textonly-primary ">
                        TEXT BUTTON
                    </button>
                </div>
            </div>
        )
    }
}

export class ButtonSizes extends Component {

    constructor(props) {
        super(props);

        this.toggle = this
            .toggle
            .bind(this);
        this.state = {
            dropdownOpen: false
        };
    }

    toggle() {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
    }

    render() {

        const buttonProps = Button.propTypes;
        console.log(buttonProps);
        return (

            <div className="container">
                <div className="row d-inline-flex align-items-center ">

                    <button
                        type="button"
                        color='secondary'
                        className="m-8 btn d-inline-flex btn-secondary">
                        CONTAINED
                    </button>
                    <button type="button" className="m-8 btn btn-sm btn-outline-primary ">CONTAINED BTN-SM</button>
                    <ButtonDropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
                        <DropdownToggle caret>
                            Button Dropdown
                        </DropdownToggle>
                        <DropdownMenu>
                            <DropdownItem header>Header</DropdownItem>
                            <DropdownItem disabled>Action</DropdownItem>
                            <DropdownItem>Another Action</DropdownItem>
                            <DropdownItem divider/>
                            <DropdownItem>Another Action</DropdownItem>
                        </DropdownMenu>
                    </ButtonDropdown>

                </div>

            </div>

        )
    }
}

export class ButtonsContained extends Component {
    constructor(props) {
        super(props);

        this.toggle = this
            .toggle
            .bind(this);
        this.state = {
            dropdownOpen: false
        };
    }

    toggle() {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
    }
    render() {
        const buttonProps = Button.propTypes;
        console.log(buttonProps);
        return (
            <div className="container">
                <div className="row">
                    <div className="col">
                        <div className="row d-inline-flex align-items-center">
                            <Button type="button" color='secondary' className="m-8 btn">
                                CONTAINED</Button>
                            <Button type="button" color='secondary' className="m-8 btn btn-sm">
                                CONTAINED SM</Button>
                            <Button type="button" className="m-8 btn btn-secondary ">
                                <i className="icon btn-icon__left prs-icon-search"/>ICON</Button>
                            <button type="button" className="m-8 btn disabled">DISABLED</button>

                            <button type="button" className="m-8 btn btn-secondary">ICON
                                <i className="btn-icon__right icon prs-icon-chevron-right"/>
                            </button>
                        </div>

                        <div className="row d-inline-flex align-items-center">
                            <ButtonDropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
                                <DropdownToggle caret>
                                    Button Dropdown
                                </DropdownToggle>
                                <DropdownMenu>
                                    <DropdownItem header>Header</DropdownItem>
                                    <DropdownItem disabled>Action</DropdownItem>
                                    <DropdownItem>Another Action</DropdownItem>
                                    <DropdownItem divider/>
                                    <DropdownItem>Another Action</DropdownItem>
                                </DropdownMenu>
                            </ButtonDropdown>
                            <button type="button" className="m-8 btn btn-primary btn-icon-only">
                                <i className="prism-icon__demo icon prs-icon-envelop5"/>
                            </button>
                            <button type="button" className="m-8 btn btn-icon-only disabled">
                                <i className="prism-icon__demo icon prs-icon-envelop5"/>
                            </button>
                            <Button type="button" color='success' className="m-8 btn">
                                SIMULCAST BID CTA</Button>
                        </div>
                    </div>

                </div>

            </div >
        )
    }
}

export class ButtonsOutlined extends Component {
    constructor(props) {
        super(props);

        this.toggle = this
            .toggle
            .bind(this);
        this.state = {
            dropdownOpen: false
        };
    }

    toggle() {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
    }

    render() {
        return (
            <div className="d-inline-flex align-items-center  row">
                <button type="button" className="m-8 btn btn-outline-primary ">CONTAINED</button>
                <button type="button" className="m-8 btn btn-outline-primary btn-sm">
                    <i className="icon btn-icon__left prs-icon-search"/>ICON</button>
                <button type="button" className="m-8 btn disabled">DISABLED</button>

                <button type="button" className="m-8 btn btn-outline-primary">ICON
                    <i className="btn-icon__right icon prs-icon-chevron-right"/>
                </button>
                <ButtonDropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
                    <DropdownToggle caret>
                        Button Dropdown
                    </DropdownToggle>
                    <DropdownMenu>
                        <DropdownItem header>Header</DropdownItem>
                        <DropdownItem disabled>Action</DropdownItem>
                        <DropdownItem>Another Action</DropdownItem>
                        <DropdownItem divider/>
                        <DropdownItem>Another Action</DropdownItem>
                    </DropdownMenu>
                </ButtonDropdown>
                <button type="button" className="m-8 btn btn-outline-primary btn-icon-only">
                    <i className=" icon prs-icon-envelop5"/>
                </button>
                <button type="button" className="m-8 btn btn-outline btn-icon-only disabled">
                    <i className=" icon prs-icon-envelop5"/>
                </button>
            </div>
        )
    }
}

export class ButtonsTextual extends Component {
    constructor(props) {
        super(props);

        this.toggle = this
            .toggle
            .bind(this);
        this.state = {
            dropdownOpen: false
        };

    }

    toggle() {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
    }
    render() {
        return (
            <div className="d-inline-flex align-items-center row">
                <button type="button" className="btn btn-textonly-primary ">TEXT BUTTON</button>
                <button type="button" className="btn btn-sm btn-textonly-primary ">TEXT SM</button>

                <button type="button" className="btn btn-textonly-primary">ICON RIGHT
                    <i className="btn-icon__right  icon prs-icon-chevron-right"/>
                </button>
                <ButtonDropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
                    <DropdownToggle caret>
                        Button Dropdown
                    </DropdownToggle>
                    <DropdownMenu>
                        <DropdownItem header>Header</DropdownItem>
                        <DropdownItem disabled>Action</DropdownItem>
                        <DropdownItem>Another Action</DropdownItem>
                        <DropdownItem divider/>
                        <DropdownItem>Another Action</DropdownItem>
                    </DropdownMenu>
                </ButtonDropdown>

                <button type="button" className="btn btn-textonly disabled">DISABLED
                </button>
                <button type="button" className="btn disabled btn-textonly-primary">ICON RIGHT
                    <i className="btn-icon__right  icon prs-icon-chevron-right"/>
                </button>
                <button
                    type="button"
                    className=" m-8 btn btn-textonly-primary  btn-primary btn-icon-only">
                    <i className="  icon prs-icon-envelop5"/>
                </button>
                <button
                    type="button"
                    className="btn m-8 btn-outline-primary btn-textonly-primary btn-icon-only disabled">
                    <i className="prism-icon__demo icon prs-icon-envelop5"/>
                </button>
            </div>
        )
    }
}

export class PageSelectors extends Component {
    render() {
        return (
            <div
                classname="demo-buttons_textonly d-flex align-items-center justify-content-center">
                <ul className="pagination">
                    <li className="page-item disabled">
                        <span className="page-link" href="#" tabIndex={-1}>
                            1
                        </span>
                    </li>
                    <li className="page-item">
                        <span className="page-link" href="#">
                            2
                        </span>
                    </li>
                    <li className="page-item">
                        <span className="page-link" href="#">
                            3
                        </span>
                    </li>
                </ul>
            </div>
        )
    }
}
