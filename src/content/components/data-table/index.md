---
layout: custom
group: components
path: /components/data-table/
title: Data Table
status: Complete
component: Data Table
heading: Data Table
role: main
description: Data Table
tabs: [Usage, Code, Style]
---
