import React, {Component} from 'react';
import Accordion from '../../../ds-components/Accordion/Accordion.js';
import AccordionItem from '../../../ds-components/Accordion/AccordionItem.js';
import ComponentDemo from '../DemoCards/ComponentDemo'
import ItemFilter from '../../../ds-components/Accordion/ItemFilter.js';

class AccordionExample extends Component {

    render(props) {

        return (
            <ComponentDemo className='prism-accordion'>
                <Accordion>
                    <AccordionItem heading="Filter Name">
                        <div className="accordion-item__body">
                            <ItemFilter/>
                        </div>
                    </AccordionItem>
                    <AccordionItem heading="Filter Name">
                        <div className="accordion-item__body">
                            <ItemFilter/>
                        </div>
                    </AccordionItem>
                    <AccordionItem heading="Filter Name">
                        <div className="accordion-item__body">
                            <ItemFilter/>
                        </div>
                    </AccordionItem>
                </Accordion>
            </ComponentDemo>

        );
    }
}

export default AccordionExample;
