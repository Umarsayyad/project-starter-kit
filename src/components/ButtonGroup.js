import React from 'react'

const ButtonGroup = ({children}) => (
  <div className="prism-btn-group">{children}</div>
)

export default ButtonGroup
