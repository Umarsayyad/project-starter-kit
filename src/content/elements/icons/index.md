---
path: /elements/icons/
layout: foundation
title: Icons
description: Icons
group: elements
tabs: [Library, Usage]
status: Complete
role: main
---

**Icons help commuicate key touch-points in the experience. They are to be used in comination with text and without text.**

# Manheim Font

The Manheim font leverages ico-moon's font library and is a css-managed web font. There are over 2000 fonts in total of which many were handpicked to be used across experiences. To use the font in your designs download the fonts to your font manager to preview and use them.

## <button class='prism-btn '>DOWNLOAD FONTS</button>

---

<prism-icons></prism-icons>
