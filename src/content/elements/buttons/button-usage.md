---
layout: custom
group: elements
path: /elements/buttons/usage
heading: Buttons
title: Buttons
description: Buttons are simple controls for executing specific on-page actions and code. Buttons are most commonly used in forms, dialogs, and modals to facilitate required user actions.
status: Complete
tabs: ['Usage', 'Code', 'Style']
---

### Definition

Buttons are used to submit or request information, make decisions, and for navigation.

Button hierarchy is established by whether a button is contained (i.e. with a background color) for most emphasis, or outlined (no background) for less emphasis, or text only for the least emphasis.

Buttons may be normal (40px high) or small (32px). Full-width buttons are only allowed on the XS grid (0-479). In all other cases, button width is determined by the length of the label plus minimum L/R padding.

---

### Common Characteristics

All buttons have bold, ALL-CAPS labels.

All buttons have these states: disabled, active, hover/tap, and focused.

<usage-demo>
<buttons-common></buttons-common>
</usage-demo>

---

---

### Size

Buttons are normally 40px high, but a small 32px alternative may be used when vertical space is at a premium. Whichever size is used, use the same size for any associated elements like inputs and selects.

Special controls like the keyword search button, pagination controls, icon buttons, and menu buttons are always 32px high.

<usage-demo>
<button-sizes></button-sizes>
</usage-demo>

---

---

### Hierarchy

#### Contained Buttons

Contained buttons have the most emphasis and should be used for primary actions. When one button is needed (such as an “OK”) use a contained button.

An element cannot have more than one contained button.

<usage-demo>
<buttons-contained></buttons-contained>
</usage-demo>

---

---

#### Outlined Buttons

Outlined buttons provide less emphasis. When two or more buttons without hierarchy are needed, use multiple outlined buttons, or multiple text buttons.

<usage-demo>
<buttons-outlined></buttons-outlined>
</usage-demo>

---

---

#### Text Buttons

Text buttons have the least emphasis. When two buttons with hierarchy are needed (e.g. “SUBMIT” OR “CANCEL”), use a contained button for the primary action, and a text button for the secondary action.

When two or more buttons without hierarchy are needed, use multiple text buttons, or multiple outlined buttons.

<usage-demo>
<buttons-text></buttons-text>
</usage-demo>

---

---

### Scenarios

#### Single Button

---

<img-wrap>
<img src='./images/SINGLECONTAINED.png' />
</img-wrap>

When a single button is needed, use a contained button.

---

#### Two Buttons with Hierarchy

---

<img-wrap>
<img src='./images/TWO-BUTTONS-HEIRARCHY.png' />
</img-wrap>

---

#### Two Buttons without Hierarchy

---

<img-wrap>
<img src='./images/TWO BUTTONS NO HEIRARCHY.png' />
<img src='./images/CARD-2-TEXT-BUTTONS.png' />
</img-wrap>

When two or more buttons without hierarchy are needed, they may be outlined buttons or buttons, but cannot be contained buttons.

---

#### Anchored Buttons

---

<img-wrap>
<img src='./images/CANCEL-SUBMIT.png' />
</img-wrap>

Buttons can be optionally anchored to the bottom of the screen on a semi-transparent action bar.

---

<img-wrap>
<img src='./images/BACK-NEXT.png' />
</img-wrap>

The text button can be optionally floated to the left.

---

<img-wrap>
<img src='./images/TWO-BUTTONS-HEIRARCHY.png' />
</img-wrap>

When stacking buttons, the higher priority button (if any) goes on top.
