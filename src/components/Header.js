import React, {Component} from 'react'
import classnames from 'classnames'
import Logo from './Logo'
import Button from './Button'
import PrimaryNav from './PrimaryNav'

export class Header extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isToggleOn: true
    }
    this.toggleMenu = this
      .toggleMenu
      .bind(this)
  }

  toggleMenu() {
    this.setState(prevState => ({
      isToggleOn: !prevState.isToggleOn
    }))
  }

  render() {

    let headerClass = classnames({
      'prism-header': true,
      'ads-u-margin-bottom-none': this.props.styleModifier === 'ads-u-margin-bottom-none',
      'prism-header--vertical': this.props.styleModifier === 'prism-header--vertical'
    })

    return (
      <div>
        <header className={headerClass} status="Complete" role="banner">

          <Button
            styleModifier={this.state.isToggleOn
            ? 'prism-btn--small prism-header__nav-btn open'
            : ' prism-btn--small prism-header__nav-btn close'}
            text={this.state.isToggleOn
            ? <span className="icon prs-icon-menu"/>
            : <span className="icon prs-icon-cross"/>}
            onClickHandler={this.toggleMenu}/>
          <Button
            styleModifier={this.state.isToggleOn
            ? ' prism-btn--small prism-header__nav-btn hide-me'
            : 'prism-btn--small prism-header__nav-btn prism-close'}
            text={< span className = "icon prs-icon-cross" />}
            onClickHandler={this.toggleMenu}/>
        </header>
        <div
          className={this.state.isToggleOn
          ? 'prism-header__nav-container'
          : 'prism-header__nav-container is-active'}>

          <div className="prism-header__inner">
            <div
              className={this.state.isToggleOn
              ? 'prism-logo-hide'
              : 'prism-logo-show'}>
              <Logo siteTitle={this.props.siteTitle}/>

            </div>
          </div>

          <PrimaryNav data={this.props.navData} toggleStatus={this.toggleMenu}/>
        </div>

      </div>

    )
  }
}

export default Header
