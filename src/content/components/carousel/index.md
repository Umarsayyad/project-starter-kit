---
layout: custom
group: components
path: /components/carousel/
title: Carousel
status: Complete
component: Carousel
heading: Carousel
role: main
description: Carousel
tabs: [Usage, Code, Style]
---
