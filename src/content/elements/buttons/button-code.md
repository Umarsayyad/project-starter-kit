---
layout: custom
group: elements
path: /elements/buttons/code
heading: Buttons
title: Buttons
description: Buttons are simple controls for executing specific on-page actions and code. Buttons are most commonly used in forms, dialogs, and modals to facilitate required user actions.
tabs: ['Usage', 'Code', 'Style']
---
___
 
###Contained Buttons

Using the ```.btn``` class without a modifier will create a contained button. The ```.btn``` class can be used on ```<button>, <a>, or <input>``` elements. Links should be assigned a role="button" attribute.
 
 
<component-demo>
<button type="button" className="btn m-8 btn-primary">TEXT BUTTON</button>
<button type="button" className="btn m-8 btn-sm btn-primary ">TEXT SM</button>
<button type="button" className="m-8 btn btn-primary btn-icon-only"><i className="icon prs-icon-envelop5"></i></button>
</component-demo>  

---

###Outlined Buttons
For outlined buttons, add a ```.btn-outline-***``` modifier followed by color. 

 <component-demo>
<button type="button" className="m-8 btn btn-outline-primary">CONTAINED</button>
 <button type="button" className="m-8 btn btn-outline-primary "><i className="icon btn-icon__left prs-icon-search"></i>ICON</button>  <button type="button" className="m-8 btn btn-outline-primary  btn-icon-only"><i className="prism-icon__demo icon prs-icon-envelop5"></i>
</button> 
</component-demo>
<hr>

#Text Buttons

<component-demo>
<button type="button" className="btn m-8 btn-textonly-primary">TEXT BUTTON</button><button type="button" className="btn btn-textonly-primary btn-sm m-8 align-self-center  ">TEXT SM</button>
<button type="button" className="m-8 btn btn-textonly-primary   btn-icon-only align-self-center "><i className="icon prs-icon-envelop5 "></i></button>
</component-demo>

---

#<span class='markdown-section-heading'> Overview</span>

#Outlined Buttons



###JavaScript
Pass these modifiers as props.

---

| Option     | Type       |
| ---------- | ---------- |
| active     | `bool`     |
| block      | `bool`     |
| color      | `string`   |
| disabled   | `bool`     |
| size       | `string`   |
| children   | `object`   |
| className  | `string`   |
| cssModule  | `object`   |
| close      | `bool`     |
| onClick    | `function` |

---


 
</div>
