---
layout: custom
group: components
subgroup: blocks-and-cards
path: /components/blocks-and-cards/hero
title: Hero
description: This is a hero.
status: Not Started
---

 
A <span class='font-light-blue'>Demo</span> provides an example. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet. Sed sit amet arcu aliquet, molestie justo at, auctor nunc. Phasellus ligula ipsum, volutpat eget semper id, viverra eget nibh. 




#Demo
<div class='component-demo-body'>
 




#<span id='usage' class='markdown-section-heading'> Overview</span>

###SCSS

Use these modifiers with `.css-className`


<md-table>
| Class            | Property |
| ---------------- | -------- |
| Some-color-thing | 545342   |
| Some-color-thing | 545342   |
| Some-color-thing | 545342   |
| Some-color-thing | 545342   |
</md-table>


###JavaScript
Access these modifiers with `options`

<md-table>
| Option        | Type     |
| ------------- | -------- |
| Disabled      | `bool`   |
| Items         | `num`    |
| Title         | `string` |
| StyleModifier | `string` |
</md-table>


##Typography
Access these modifiers with `options`

<md-table>
| Class            | Property |
| ---------------- | -------- |
| Some-color-thing | 545342   |
| Some-color-thing | 545342   |
| Some-color-thing | 545342   |
| Some-color-thing | 545342   |
</md-table>
</div>