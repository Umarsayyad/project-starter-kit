import React from 'react'
import PrimaryNavItem from './PrimaryNavItem'
import PrimarySubNavItem from './PrimarySubNavItem'

const componentList = []
const PrimaryNav = ({data, toggleStatus}) => (

  <nav className="prism-primary-nav" role="navigation">
    <ul
      className="prism-primary-nav__list"
      role="tablist"
      aria-multiselectable="true">
      <PrimaryNavItem href="/whats-new" label="Updates"/>
      <PrimaryNavItem
        href="/getting-started"
        label="Getting Started"
        group='getting-started'
        toggleStatus={toggleStatus}/>

      <PrimaryNavItem
        label="Foundation"
        group='foundation'
        toggleStatus={toggleStatus}
        subNavItem=
        { <ul className = "prism-primary-nav__sublist" > { data.filter(post => post.node.frontmatter.group === 'foundation').map(({node: post}) => { return (<PrimarySubNavItem key={post.id} title={post.frontmatter.title} url={post.frontmatter.path} toggleStatus={toggleStatus}/>); }) } </ul>}/>

      <PrimaryNavItem
        href="/elements"
        label="Elements"
        group='elements'
        toggleStatus={toggleStatus}
        subNavItem={< ul className = "prism-primary-nav__sublist" > {
        data
          .filter(post => post.node.frontmatter.group === 'elements')
          .filter(post => post.node.frontmatter.status === 'Complete')
          .map(({node: post}) => {
            return (<PrimarySubNavItem
              key={post.id}
              title={post.frontmatter.title}
              url={post.frontmatter.path}
              toggleStatus={toggleStatus}/>)
          })
      } < /ul>}/>

      <PrimaryNavItem
        href="/components"
        label="Components"
        group='components'
        toggleStatus={toggleStatus}
        subNavItem={< ul className = "prism-primary-nav__sublist" > {
        data
          .filter(post => post.node.frontmatter.group === 'components')
          .filter(post => post.node.frontmatter.status === 'Complete')
          .map(({node: post}) => {
            return (<PrimarySubNavItem
              key={post.id}
              title={post.frontmatter.title}
              url={post.frontmatter.path}
              toggleStatus={toggleStatus}/>)
          })
      } < /ul>}/>
      <PrimaryNavItem
        href="/patterns"
        label="Patterns"
        group='patterns'
        toggleStatus={toggleStatus}
        subNavItem={< ul className = "prism-primary-nav__sublist" > {
        data
          .filter(post => post.node.frontmatter.group === 'patterns')
          .filter(post => post.node.frontmatter.layout === 'utilities-index')
          .map(({node: post}) => {
            return (<PrimarySubNavItem
              key={post.id}
              title="Overview"
              url={post.frontmatter.path}
              toggleStatus={toggleStatus}
              sub/>)
          })
      }
      {
        data
          .filter(post => post.node.frontmatter.group === 'patterns')
          .filter(post => post.node.frontmatter.layout === 'page')
          .filter(post => post.node.frontmatter.status === 'Complete')
          .map(({node: post}) => {
            return (<PrimarySubNavItem
              key={post.id}
              title={post.frontmatter.title}
              url={post.frontmatter.path}
              toggleStatus={toggleStatus}/>)
          })
      } < /ul>}/>
      <PrimaryNavItem
        href="/page-templates"
        group='page-templates'
        label="Page Templates"
        toggleStatus={toggleStatus}/>

      <PrimaryNavItem
        href="/ui-kits"
        group='ui-kits'
        label="UI Kits"
        toggleStatus={toggleStatus}/>

    </ul>
  </nav>
)

export default PrimaryNav
