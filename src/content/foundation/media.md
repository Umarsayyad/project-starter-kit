---
path: /foundation/media
layout: page
type: detail
title: Media
group: foundation
status: Incomplete
description: How the style guide uses motion to enhance the user experience.
---

For inspiration, check out [Material Design's thorough motion guidelines](https://material.io/guidelines/motion/material-motion.html#material-motion-why-does-motion-matter), as well as all of [Val Head](http://valhead.com/)'s fantastic resources around UI animation.
