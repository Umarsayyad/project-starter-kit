import React from 'react'

const Table = ({tableHeaderColumns, tbody, styleModifier}) => {
  return (
    <table className={'prism-table ' + styleModifier}>
      <thead className="prism-table__header">
        <tr className="prism-table__header-row">
          {tableHeaderColumns.map((tableHeaderColumn, index) => (
            <th scope="col" className="prism-table__header-cell" key={index}>
              {tableHeaderColumn}
            </th>
          ))}
        </tr>
      </thead>
      <tbody className="prism-table__body">{tbody}</tbody>
    </table>
  )
}

export default Table
