import React from 'react'
import markdownIt from 'markdown-it'

const Card = ({href, header, title, description, footer}) => {
  const md = markdownIt({html: true, linkify: true})
  return (
    <a href={href} className="prism-card">
      {header && <header className="prism-card__header">{header}</header>}

      <div className="prism-card__body">
        <h2 className="prism-card__title">
          <span className="prism-card__title-text">{title}</span>
        </h2>

        {description && (<div
          className="prism-card__desc"
          dangerouslySetInnerHTML={{
          __html: md.render(description)
        }}/>)}
      </div>

      {footer && <div className="prism-card__footer">{footer}</div>}
    </a>
  )
}

export default Card
