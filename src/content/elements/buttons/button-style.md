---
layout: custom
group: elements
path: /elements/buttons/style
heading: Buttons
title: Buttons
description: Buttons are simple controls for executing specific on-page actions and code. Buttons are most commonly used in forms, dialogs, and modals to facilitate required user actions.
tabs: ['Usage', 'Code', 'Style']
---

###SCSS

Use these modifiers with `.css-className`

#### Color 
 
| Class          | Property                        |
| -------------- | ------------------------------- |
| .btn-primary   | Applies primary button styles   |
| .btn-secondary | Applies secondary button styles |
| .btn-danger    | Applies primary button styles   |
| .btn-success   | Applies secondary button styles |
| .btn-simulcast | Applies simulcast button styles |
 
---

#### Size
 

| Class   | Property                    |
| ------- | --------------------------- |
| .btn-sm | Applies small button styles |
 


 
#### Type
 
 
---

 
| Class                | Property         |
| -------------------- | ---------------- |
| .btn-outline        | Outlined Button  |
| .btn-textonly       | Text-Only Button |
| .btn-icon-only       | Icon-Only Button |
| .disabled, :disabled | Disabled Styles  |
| .btn.dropdown-toggle | Dropdown Button  |
| .btn-floating        | Floating Button  |
| .btn-pagination      | Pagination Button|

---
---
 
