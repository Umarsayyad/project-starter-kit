import React, {Component} from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'

export default class NumberInputGroup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 0,
            max: 50
        }
        this.handleIncrement = this
            .handleIncrement
            .bind(this);
        this.handleDecrement = this
            .handleDecrement
            .bind(this);
        this.handleChange = this
            .handleChange
            .bind(this);
    }

    componentDidMount() {
        this.setState({max: this.props.number})
    }
    handleIncrement(event) {
        console.log()
        let val = this.state.value;
        let newVal = val += 1;

        console.log(val);
        this.setState({value: newVal});
        event.preventDefault();
    }
    handleDecrement(event) {
        let val = this.state.value;
        let newVal = val -= 1;

        console.log(val);
        this.setState({value: newVal});

        event.preventDefault();
    }
    handleChange() {}

    render() {

        const {value} = this.state;
        const {name, number} = this.props;
        return (
            <div>
                <label htmlFor="numberInput">{name}</label>
                <div className="input-group  number-input__group">
                    <div className="input-group-prepend">
                        <button
                            onClick={this.handleDecrement}
                            className="btn btn-primary"
                            id="basic-addon1">-</button>
                    </div>
                    <input
                        onChange={this.handleChange()}
                        type="text"
                        className="form-control"
                        id='numberInput'
                        value={value}
                        placeholder="0"
                        aria-label="number input"
                        aria-describedby="basic-addon1"/>
                    <div className="input-group-append">
                        <button
                            className="btn btn-primary"
                            onClick={this.handleIncrement}
                            id="basic-addon1">
                            <i className='icon prs-icon-plus3'/>
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}

NumberInputGroup.propTypes = {
    name: PropTypes.string,
    number: PropTypes.number
};
