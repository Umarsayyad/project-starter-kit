import React, {Component} from 'react';
import PropTypes from 'prop-types';
import CopyToClipboard from 'react-copy-to-clipboard';
import {Icon} from '../Icon.js';

class IconCard extends Component {
  static propTypes = {
    name: PropTypes.string,
    height: PropTypes.string,
    width: PropTypes.string,
    viewBox: PropTypes.string,
    svgString: PropTypes.string
  };

  static defaultProps = {
    name: '',
    height: '16',
    width: '16',
    viewBox: '1024',
    svgString: ''
  };

  state = {
    displayCopied: false
  };

  toggleCopied = () => {
    this.setState({displayCopied: true});
    setTimeout(() => {
      this.setState({displayCopied: false});
    }, 2500);
  };

  handleFocus = () => {
    this
      .iconActions
      .classList
      .add('displayed');
  };

  handleBlur = () => {
    this
      .iconActions
      .classList
      .remove('displayed');
  };

  render() {
    const {name, height, width, viewBox, svgString} = this.props;

    return (
      <div className="icon">
        <div tabIndex={0} className="icon__card">
          <Icon
            name={name}
            styleModifier="icon-demo__icon"
            description={name}
            height={height}
            width={width}
            viewBox={viewBox}
            tabIndex={-1}
            alt={name}/>
          <div
            ref={iconActions => {
            this.iconActions = iconActions;
          }}
            className="icon__actions"
            onBlur={this.handleBlur}>
            <CopyToClipboard text={svgString} onCopy={this.toggleCopied}>
              <button onFocus={this.handleFocus} tabIndex={0} className="icon-button">
                {this.state.displayCopied
                  ? 'Icon Copied!'
                  : 'Copy Icon'}
              </button>
            </CopyToClipboard>

          </div>
        </div>
        <h5>{name}</h5>
        <span>
          {`#${name}`}</span>
      </div>
    );
  }
}

export default IconCard;
