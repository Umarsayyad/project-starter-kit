---
path: /foundation/typography
layout: foundation
type: detail
title: Typography
group: foundation
description: Typography style description
status: Complete
tabs: []
---

**Body (M) 16pt Rob Reg, consectetur accordian adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet. Sed sit amet arcu aliquet, molestie justo at, auctor nunc. Phasellus ligula ipsum, volutpa, viverra eget nibh.**

---

#Roboto & Roboto Condensed

---

Body (M) 16pt Rob Reg, consectetur accordian adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet. Sed sit amet arcu aliquet, molestie justo at, auctor nunc. Phasellus ligula ipsum, volutpa, viverra eget nibh.

<typography-table></typography-table>
