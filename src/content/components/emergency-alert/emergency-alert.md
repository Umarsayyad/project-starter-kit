---
layout: custom
group: components
path: /components/emergency-alert/
title: Emergency Alert
status: Complete
component: Emergency-Alert
heading: Emergency Alert
role: main
description: Emergency Alert
tabs: [Usage, Code, Style]
---
