import React, {Component} from 'react';
import Markdown from 'markdown-to-jsx';
import Prism from 'prismjs';
import classnames from 'classnames';
import {PrismCode} from 'react-prism';
import reactElementToJSXString from 'react-element-to-jsx-string';
import ReactDOMServer from 'react-dom/server';

import {
    Card,
    CardBody,
    CardTitle,
    CardText,
    CardHeader,
    CardFooter,
    Button,
    TabContent,
    TabPane,
    Nav,
    NavItem,
    NavLink,
    Row,
    Col
} from 'reactstrap';
import jsxToString from 'jsx-to-string'
class ComponentUsageDemo extends Component {
    constructor(props) {
        super(props);

        this.toggle = this
            .toggle
            .bind(this);
        this.state = {
            activeTab: '1'
        };
    }

    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({activeTab: tab});
        }
    }
    render() {

        return (
            <div className='demo'>
                <Card>
                    <CardHeader className='component-demo__box'>
                        {this.props.children}
                    </CardHeader>

                </Card>
            </div>
        );
    }
}

export default ComponentUsageDemo;