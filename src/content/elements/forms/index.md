---
layout: custom
group: elements
path: /elements/form-elements/
heading: Forms
title: Forms
description: Buttons are simple controls for executing specific on-page actions and code. Buttons are most commonly used in forms, dialogs, and modals to facilitate required user actions.
tabs: ['Usage', 'Code', 'Style']
---

#Forms
<usage-demo>
<basic-forms></basic-forms>
</usage-demo>
