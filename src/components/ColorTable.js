import React from 'react'
import Table from './Table'

const ColorTable = (tableHeader, tbody) => {
    return (
        <table className={'prism-table '}>
            <thead className="prism-table__header">
                <tr className="prism-table__header-row">

                    <th className="prism-table__header-cell" key={index}>
                        {tableHeader}
                    </th>
                </tr>
            </thead>
            <tbody className="prism-table__body">{tbody}</tbody>
        </table>
    )
}

export default ColorTable