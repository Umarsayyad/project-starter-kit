import React, {Component} from 'react';
import {FormGroup, Label, Input} from 'reactstrap';

class ItemFilter extends Component {

    render() {
        const heading = this.props.heading;

        return (
            <FormGroup className="filter-group">
                <FormGroup check>
                    <Label className='checkbox--custom' check>
                        <div><Input type="checkbox" name="check1"/> {' '}
                            <span className='filterName'>Filter Item</span>
                        </div>
                        <span className='filterNum'>#,###</span>
                    </Label>
                </FormGroup>
                <FormGroup check>
                    <Label className='checkbox--custom' check>
                        <div><Input type="checkbox" name="check1"/> {' '}
                            <span className='filterName'>Filter Item</span>
                        </div>
                        <span className='filterNum'>#,###</span>
                    </Label>
                </FormGroup>
                <FormGroup check>
                    <ul>
                        <Label className='checkbox--custom' check>
                            <div><Input type="checkbox" name="check1"/> {' '}
                                <span className='filterName'>Filter Item</span>
                            </div>
                            <span className='filterNum'>#,###</span>
                        </Label>
                        <ul className='sub-filter'>
                            <FormGroup check>
                                <Label className='checkbox--custom' check>
                                    <div><Input type="checkbox" name="check1"/> {' '}
                                        <span className='filterName'>Filter Item</span>
                                    </div>
                                    <span className='filterNum'>#,###</span>
                                </Label>
                            </FormGroup>
                            <FormGroup check>
                                <Label className='checkbox--custom' check>
                                    <div><Input type="checkbox" name="check1"/> {' '}
                                        <span className='filterName'>Filter Item</span>
                                    </div>
                                    <span className='filterNum'>#,###</span>
                                </Label>
                            </FormGroup>
                            <FormGroup check>
                                <Label className='checkbox--custom' check>
                                    <div><Input type="checkbox" name="check1"/> {' '}
                                        <span className='filterName'>Filter Item</span>
                                    </div>
                                    <span className='filterNum'>#,###</span>
                                </Label>
                            </FormGroup>
                        </ul>
                    </ul>
                </FormGroup>
            </FormGroup>
        );
    }
}

export default ItemFilter;
