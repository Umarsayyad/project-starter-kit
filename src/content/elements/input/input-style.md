---
path: /elements/input/style
layout: custom
title: Text Inputs
description: Input
group: elements
tabs: ['Usage', 'Code', 'Style']
---

# Style

### Size

Full-width inputs are only allowed on the XS grid (0-479). In all other cases, inputs must span to a specified number of columns in the layout. Text inputs are normally 40px high, but a small 32px alternative may be used. Whichever size is used, use the same size for other form elements like selects and buttons.

## States

---

###Disabled

<usage-demo>
               <div className="input-demo__card card container">
        <form>
            <div className="form-row">
                <div className="col form-group">
                    <label htmlFor="formGroupExampleInput">Label
                    </label>
                    <input disabled
                        type="text"
                        className="form-control"
                        id="formGroupExampleInput"
                        placeholder="Form Control Regular"/>
                </div>
            </div>
            <div className="form-row">
                <div className="col form-group">
                    <label htmlFor="formGroupExampleInput3">Label</label>
                    <input disabled
                        type="text"
                        className="form-control-sm form-control "
                        id="formGroupExampleInput3"
                        placeholder="Form Contol Small"/>
                </div>
            </div>
        </form>
    </div>

</usage-demo>

---

###Active

<usage-demo>
               <div className="input-demo__card card container">
        <form>
            <div className="form-row">
                <div className="col form-group">
                    <label htmlFor="formGroupExampleInput">Label
                    </label>
                    <input 
                    value='active typ...'
                        type="text"
                        className="focus active form-control"
                        id="formGroupExampleInput"
                        placeholder="Form Control Regular"/>
                </div>
            </div>
            <div className="form-row">
                <div className="col form-group">
                    <label htmlFor="formGroupExampleInput3">Label</label>
                    <input   value='active typ...'
                        type="text"
                        className="focus form-control-sm form-control "
                        id="formGroupExampleInput3"
                        placeholder="Form Contol Small"/>
                </div>
            </div>
        </form>
    </div>

</usage-demo>

---

###Focus

<usage-demo>
               <div className="input-demo__card card container">
        <form>
            <div className="form-row">
                <div className="col form-group">
                    <label htmlFor="formGroupExampleInput">Label
                    </label>
                    <input  
                        type="text"
                        className="focus active form-control"
                        id="formGroupExampleInput"
                        placeholder="Form Control Regular"/>
                </div>
            </div>
            <div className="form-row">
                <div className="col form-group">
                    <label htmlFor="formGroupExampleInput3">Label</label>
                    <input  
                        type="text"
                        className="focus form-control-sm form-control "
                        id="formGroupExampleInput3"
                        placeholder="Form Contol Small"/>
                </div>
            </div>
        </form>
    </div>

</usage-demo>

---

###Validation

<usage-demo><form-validation></form-input></usage-demo>
