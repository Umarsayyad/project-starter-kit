module.exports = {
  pathPrefix: `/`,
  siteMetadata: {
    author: 'Umar Sayyad',
    title: `Manheim Styleguide`
  },
  plugins: [

    'gatsby-plugin-catch-links', {
      resolve: 'gatsby-source-filesystem',
      options: {
        path: `${__dirname}/src`,
        name: 'src'
      }
    }, {
      resolve: 'gatsby-transformer-remark',
      options: {
        plugins: [
          'gatsby-remark-images', {
            resolve: "gatsby-remark-embed-snippet",
            options: {
              // Class prefix for <pre> tags containing syntax highlighting; defaults to
              // 'language-' (eg <pre class="language-js">). If your site loads Prism into the
              // browser at runtime, (eg for use with libraries like react-live), you may use
              // this to prevent Prism from re-processing syntax. This is an uncommon
              // use-case though; If you're unsure, it's best to use the default value.
              classPrefix: "language-",

              // Example code links are relative to this dir. eg examples/path/to/file.js
              directory: `${__dirname}/src/examples/`
            }
          }, {
            resolve: `gatsby-remark-prismjs`,
            options: {
              // Class prefix for <pre> tags containing syntax highlighting; defaults to
              // 'language-' (eg <pre class="language-js">). If your site loads Prism into the
              // browser at runtime, (eg for use with libraries like react-live), you may use
              // this to prevent Prism from re-processing syntax. This is an uncommon
              // use-case though; If you're unsure, it's best to use the default value.
              classPrefix: "language-"
            }
          },
          "gatsby-remark-component"
        ]
      }
    },
    `gatsby-transformer-json`,
    'gatsby-plugin-sharp',
    'gatsby-plugin-react-helmet'
  ]
}
