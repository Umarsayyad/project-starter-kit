---
layout: custom
group: components
path: /components/progress-indicator/
title: Progress Indicator
status: Complete
component: Progress-Indicator
heading: Progress Indicator
role: main
description: Progress Indicator
tabs: [Usage, Code, Style]
---
