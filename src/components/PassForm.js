import React, {Component} from 'react'
import classNames from 'classnames';
import Icon from './Icon.js';

const blocked = () => <Icon name='eye-blocked'/>
const eye = () => <Icon name='eye'/>

export default class PassForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showPass: false
        }

        this.togglePasswordMask = this
            .togglePasswordMask
            .bind(this);
    }
    togglePasswordMask() {
        console.log('clicked')
        this.setState(prevState => ({
            showPass: !prevState.showPass
        }));
    };
    render() {
        const icnClass = classNames('text-input__icon-right', 'prism-icon-md', 'password-toggle__icon', {
            'prs-icon-eye-blocked': !this.state.showPass,
            ' prs-icon-eye icon-primary': this.state.showPass
        });

        const {showPass} = this.state
        return (

            <div className="d-flex align-items-center inline">
                <input
                    type={showPass
                    ? 'text'
                    : 'password'}
                    className="form-control text-input__has-icon-right"
                    placeholder="Password"/>
                <span className='pr-2' onClick={this.togglePasswordMask}>
                    <Icon
                        styleModifier={icnClass}
                        name={showPass
                        ? ('eye')
                        : ('eye-blocked')}/>
                </span>

            </div>
        )
    }
}
