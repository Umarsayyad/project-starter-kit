import React, {Component} from 'react';

class TypographyTable extends Component {
    render() {
        const typeFace = [
            {
                'style': 'h1-condensed-bold',
                'tag': <h1>H1</h1>,
                'size': "26px / 2.6rem",
                'type': "Roboto Condensed",
                'weight': "Bold / 700",
                'case': "Uppercase",
                'class': '.prism-h1-condensed',
                'lineHeight': '2.8rem'
            }, {
                'style': 'h2',
                'tag': <h2>H2</h2>,
                'size': "22px / 2.2rem",
                'type': "Roboto",
                'weight': "Light / 300",
                'case': "Uppercase",
                'class': '.prism-h2',
                'lineHeight': '2.6rem'
            }, {
                'style': 'h3',
                'tag': <h3>H3</h3>,
                'size': "20px / 2rem",
                'type': "Roboto",
                'weight': "Bold / 700",
                'case': "Uppercase",
                'class': '.prism-h3',
                'lineHeight': '2.4rem'
            }, {
                'style': 'h4',
                'tag': <h4>H4</h4>,
                'size': "18px / 1.8rem",
                'type': "Roboto",
                'weight': "Light / 300",
                'case': "Uppercase",
                'class': '.prism-h4',
                'lineHeight': '2.2rem'
            }, {
                'style': 'h5',
                'tag': <h5>H5</h5>,
                'size': "14px / 1.4rem",
                'type': "Roboto",
                'weight': "Bold / 700",
                'case': "Uppercase",
                'class': '.prism-h5',
                'lineHeight': '1.8rem'
            }, {
                'style': 'p-large',
                'tag': <p className='p-large'>P (Large)</p>,
                'size': "16px / 1.6rem",
                'type': "Roboto",
                'weight': "Regular / 400",
                'case': "Uppercase",
                'class': '.prism-p-large',
                'lineHeight': '2.4rem'
            }, {
                'style': 'p-med',
                'tag': <p className='p-medium'>P (Medium)</p>,
                'size': "14px / 1.4rem",
                'type': "Roboto",
                'weight': "Regular / 400",
                'case': "Uppercase",
                'class': '.prism-p-med',
                'lineHeight': '1.8rem'
            }, {
                'style': 'p-sm',
                'tag': <p className='p-sm'>P (Small)</p>,
                'size': "12px / 1.2rem",
                'type': "Roboto",
                'weight': "Regular / 400",
                'case': "Uppercase",
                'class': '.prism-p-sm',
                'lineHeight': '1.5rem'
            }, {
                'style': 'p-xs',
                'tag': <p className='p-xs'>P (X-Small)</p>,
                'size': "10px / 1rem",
                'type': "Roboto",
                'weight': "Regular / 400",
                'case': "Uppercase",
                'class': '.prism-p-sm',
                'lineHeight': '1.2rem'
            }

        ];
        const modifiers = [
            {
                'style': 'font-weight-regular',
                'tag': <h5 className='font-weight-regular'>H5</h5>,
                'size': "26px / 2.6rem",
                'type': "Roboto Condensed",
                'weight': "Regular / 700",
                'case': "Uppercase",
                'class': '.prism-h1-condensed',
                'lineHeight': '2.6rem'
            }, {
                'style': 'p-large font-weight-bold',
                'tag': <p className='p-large font-weight-bold'>P(Large) + Bold
                </p>,
                'size': "16px / 1.6rem",
                'type': "Roboto",
                'weight': "Bold / 700",
                'case': "Uppercase",
                'class': '.prism-p-large font-weight-bold',
                'lineHeight': '1.8rem'
            }, {
                'style': 'font-size-med font-weight-bold',
                'tag': <p className='font-weight-bold'>P(Medium) + Bold
                </p>,
                'size': "14px / 1.6rem",
                'type': "Roboto",
                'weight': "Bold / 700",
                'case': "Uppercase",
                'class': '.font-size-med font-weight-bold',
                'lineHeight': '2.4rem'
            }, {
                'style': 'font-size-small font-weight-bold',
                'tag': <p className='p-sm font-weight-bold'>P (Small)</p>,
                'size': "12px / 1.2rem",
                'type': "Roboto",
                'weight': "Regular / 700",
                'case': "Uppercase",
                'class': ' .font-size-small .font-weight-bold',
                'lineHeight': '1.8rem'
            }, {
                'style': 'font-size-xs',
                'tag': <p className='font-size-xs font-weight-bold'>P (X-Small)</p>,
                'sizePx': "10px / 1rem",
                'type': "Roboto",
                'weight': "Bold / 700",
                'case': "Uppercase",
                'class': '.font-size-xs font-weight-bold',
                'lineHeight': '1.2rem'
            }
        ]

        // const tbody = typeFace.map(type => {     return type });
        return (
            <div className="typography-table">
                <a class='btn btn-primary'>Download Fonts</a>
                <hr/><hr/>
                <table>
                    <thead>
                        <tr>
                            <th
                                style={{
                                width: '350px'
                            }}>
                                Example
                            </th>
                            <th>
                                Specs
                            </th>

                        </tr>
                    </thead>
                    <tbody>
                        {typeFace.map((item) => {
                            return (
                                <tr>
                                    <td className={item.style}>{item.tag}</td>
                                    <td>{item.type}
                                        <p>font-size: {item.size}</p>
                                        <p>font-weight: {item.weight}</p>
                                        <p>case: {item.case}</p>
                                        <p>line-height: {item.lineHeight}</p>
                                        <span className='font-color-primary'>{item.class}</span>
                                    </td>
                                </tr>
                            )
                        })}

                    </tbody>

                </table>
                <hr/>
                <table>
                    <thead>
                        <tr>
                            <th
                                style={{
                                width: '350px'
                            }}>
                                Modifiers
                            </th>
                            <th>
                                Specs
                            </th>

                        </tr>
                    </thead>
                    <tbody>
                        {modifiers.map((item) => {
                            return (
                                <tr>
                                    <td className={item.style}>{item.tag}</td>
                                    <td>{item.type}<br/>
                                        <p>font-size: {item.size}</p>
                                        <p>font-weight: {item.weight}</p>
                                        <p>case: {item.case}</p>
                                        <p>line-height: {item.lineHeight}</p>
                                        <span className='font-color-primary'>{item.class}</span>
                                    </td>
                                </tr>
                            )
                        })}

                    </tbody>

                </table>

            </div>
        );
    }
}

export default TypographyTable;
