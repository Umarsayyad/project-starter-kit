---
path: /foundation/colors/
layout: foundation
title: Colors
description: DS colors
group: foundation
status: Complete
---

##Primaries
<flex-group>
<color-card name='Prism Navy' variable="$prs-navy" hex="#003468"></color-card>
<color-card name='Prism Navy Dark' variable="$prs-navy-dark" hex="#001b35"></color-card>
<color-card name='Prism Primary Blue' variable="$prs-primary" hex="#005ba8"></color-card>
<color-card name='Prism Primary Dark' variable="$prs-primary-dark" hex="#004986"></color-card>
<color-card name='Prism Cerelean' variable="$prs-cerelean" hex="#2c90cc"></color-card>
<color-card name='Prism Cerelean Dark' variable="$prs-cerelean-dark" hex="#2372a2"></color-card>
</flex-group>
<flex-group>
<color-card name='Prism Saffron' variable="$prs-saffron" hex="#eb9114"></color-card>
<color-card name='Prism Saffron Dark'variable="$prs-saffron-dark" hex="#c0650c"></color-card>
</flex-group>

##Secondaries
<flex-group>
<color-card name='Prism Cerelean Light' variable="$prs-cerelean-light" hex="#c1dff2"></color-card>
<color-card name='Prism Gold Light' variable="$prs-gold-light" hex="#fff0c3"></color-card>
<color-card name='Prism Meadow Light' variable="$prs-meadow-light" hex="#e3fad1"></color-card>
<color-card name='Prism Scarlet Light' variable="$prs-scarlet-light" hex="#fcccc0"></color-card>
</flex-group>

##Neutrals
<flex-group>
<color-card name='Prism White' variable="$prs-white" hex="#ffffff"></color-card>
<color-card name='Prism Ash' variable="$prs-ash" hex="#eeeeee"></color-card>
<color-card name='Prism Smoke' variable="$prs-smoke" hex="#babcbe"></color-card>
<color-card name='Prism Charcoal' variable="$prs-charcoal" hex="#4a4a4a"></color-card>
<color-card name='Prism Black' variable="$prs-black" hex="#000000"></color-card>
</flex-group>

##Accents
<flex-group>
<color-card name='Prism Ocean' variable="$prs-ocean" hex="#00aaa8"></color-card>
<color-card name='Prism Jungle' variable="$prs-jungle" hex="#0d8240"></color-card>
<color-card name='Prism Meadow' variable="$prs-meadow" hex="#82c250"></color-card>
<color-card name='Prism Plum' variable="$prs-plum" hex="#8c1d58"></color-card>
<color-card name='Prism Gold' variable="$prs-gold" hex="#ffc20e"></color-card>
<color-card name='Prism Scarlet Dark' variable="$prs-scarlet-dark" hex="#902b00"></color-card>
<color-card name='Prism Scarlet' variable="$prs-scarlet" hex="#c33a00"></color-card>
</flex-group>
