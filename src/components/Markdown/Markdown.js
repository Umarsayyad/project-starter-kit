import React from 'react';
import Markdown from 'markdown-to-jsx';

export class FlexGroup extends React.Component {
    render() {
        return <div className="flex-group">{this.props.children}</div>;
    }
}

export class MdTable extends React.Component {
    render() {
        const md = this
            .props
            .children
            .toString();

        return (
            <Markdown className="md-table">
                {md}
            </Markdown>
        );
    }
}

export class p extends React.Component {
    render() {
        return <p className="markdown-text">{this.props.children}</p>;
    }
}

export class h1 extends React.Component {
    render() {
        return <h1 className="markdown-h1">{this.props.children}</h1>;
    }
}
export class h3 extends React.Component {
    render() {
        return <h3 className="markdown-h3 text-primary">{this.props.children}</h3>;
    }
}
export class h4 extends React.Component {
    render() {
        return <h4 className="markdown-h4">{this.props.children}</h4>;
    }
}
export class imgCaption extends React.Component {
    render() {
        return <span className="markdown-caption">{this.props.children}</span>;
    }
}

export class h1Condensed extends React.Component {
    render() {
        return <h1 id="h1-condensed-bold">{this.props.children}</h1>;
    }
}

export class table extends React.Component {
    render() {
        return <table class="table">{this.props.children}</table>;
    }
}
export class th extends React.Component {
    render() {
        return <th scope="col">{this.props.children}</th>;
    }
}

export class imgWrap extends React.Component {
    render() {
        return <div className="markdown-img__wrap card-header">
            <div className="image-size">
                {this.props.children}
            </div>
        </div>;
    }
}
