import React, {Component} from 'react';

class UsageTab extends Component {

    render() {
        return (
            <div className="usage-tab">
                <div className="row">
                    <div className="col">
                        {this.props.children}
                    </div>
                </div>
            </div>

        );
    }
}

export default UsageTab;
