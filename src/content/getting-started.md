---
path: /getting-started
layout: page
title: Getting Started
description: The getting started page should provide information for how to get up and running with the design system.
tags: ['guideline', 'getting started']
---

## Downloading

Source code is available on github.

<div><a href="https://ghe.coxautoinc.com/Jeff-Cameron/manheim-ds/" class="c-btn">Github</a></div>
