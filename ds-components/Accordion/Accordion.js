import React, {Component} from 'react';

class Accordion extends Component {

    render() {
        return (

            <ul className='prs-accordion'>
                {this.props.children}
            </ul>

        );
    }
}

export default Accordion;
