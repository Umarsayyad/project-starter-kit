import React, {Component} from 'react'
import {graphql} from 'gatsby'
import Helmet from 'react-helmet'
import {FaChevronLeft, FaChevronRight} from 'react-icons/fa'
import Layout from '../components/layout'
import ComponentDetailHeader from '../components/ComponentDetailHeader'
import Tabs from '../components/Tabs'
import ComponentExample from '../components/ComponentExample'
import markdownIt from 'markdown-it'
import ColorCard from '../components/ColorCard'
import rehypeReact from 'rehype-react'
import ComponentDemoMd from '../examples/DemoCards/ComponentDemoMd'
import ComponentLive from '../examples/DemoCards/ComponentLive'
import ComponentDemoVariants from '../examples/DemoCards/ComponentDemoVariants';
import {ButtonsContained, ButtonsOutlined} from '../examples/Buttons/ButtonsExample.js'
import TypographyTable from '../components/Typography/TypographyTable'
import PrismIcons from '../components/PrismIcons.js';
import {
  FlexGroup,
  MdTable,
  p,
  h1,
  h3,
  h4,
  imgCaption,
  h1Condensed,
  imgWrap
} from '../components/Markdown/Markdown'
import * as Examples from '../examples/'
import * as DsComponents from '../../ds-components/';

const renderAst = new rehypeReact({
  createElement: React.createElement,
  components: {
    'input': DsComponents.Input,
    'button': DsComponents.Button,
    'dropdown': Examples.Dropdown,
    "flex-group": FlexGroup,
    "color-card": ColorCard,
    "typography-table": TypographyTable,
    "md-table": MdTable,
    "prism-accordion": Examples.AccordionExample,
    "p": p,
    "h1": h1,
    'h3': h3,
    "h4": h4,
    "h1-condensed": h1Condensed,
    "buttons-common": Examples.ButtonsCommon,
    "buttons-contained": Examples.ButtonsContained,
    "buttons-outlined": Examples.ButtonsOutlined,
    'prism-icons': PrismIcons,
    'accordion-usage': Examples.AccordionUsage,
    'img-caption': imgCaption,
    'component-demo': ComponentDemoMd,
    'component-live': ComponentLive,
    'usage-demo': Examples.ComponentUsageDemo,
    'basic-forms': Examples.BasicForms,
    'variant-demo': ComponentDemoVariants,
    'buttons-text': Examples.ButtonsTextual,
    'page-selectors': Examples.PageSelectors,
    'alert-example': Examples.AlertExample,
    'check-radio': Examples.CBradioExample,
    'text-input': Examples.TextInput,
    'form-validation': Examples.FormValidation,
    'modal-heading': Examples.ModalHeading,
    'modal-noheading': Examples.ModalNoHeading,
    'form-optional': Examples.FormOptional,
    'form-inline': Examples.FormInlineInput,
    'tab-example': Examples.TabExample,
    'input-icon': Examples.TextInputIcon,
    'button-sizes': Examples.ButtonSizes,
    'number-input': Examples.NumberInput,
    'drop-down': Examples.Dropdown,
    'img-wrap': imgWrap

  }
}).Compiler

export class Custom extends Component {
  componentDidMount() {
    const script = document.createElement('script')
    script.src = '../../vendor/clipboard.min.js'
    document
      .body
      .appendChild(script)

    const scriptCopy = document.createElement('script')
    scriptCopy.src = '../../vendor/copy.js'
    document
      .body
      .appendChild(scriptCopy)
  }

  render() {

    const md = markdownIt({html: true, linkify: true})
    const {markdownRemark: post} = this.props.data
    const {next, prev} = this.props.pageContext

    return (

      <Layout>
        <Helmet title={`${post.frontmatter.title}`}/>
        <ComponentDetailHeader
          title={post.frontmatter.title}
          description={post.frontmatter.description}
          status={post.frontmatter.status}
          group={post.frontmatter.group}/> {post.frontmatter.tabs && (<Tabs tabs={post.frontmatter.tabs} styleModifier='component-tabs'/>)}
        <div className='component-tab'>
          <div className="l-container markdown">
            {renderAst(post.htmlAst)}
          </div>
        </div>
      </Layout>
    )
  }
}

export default Custom

export const CustomQuery = graphql `
  query customQuery($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      htmlAst
      frontmatter {
        path
        tags
        title
        component
        description
        status
        group
        subgroup
        tabs
        role 
        usage {
          title
          description
          demo
        }
      }
    }
  }
`
