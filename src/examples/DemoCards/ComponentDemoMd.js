import React, {Component} from 'react';
import Markdown from 'markdown-to-jsx';
import classnames from 'classnames';
import {PrismCode} from 'react-prism';
import ReactDOMServer from 'react-dom/server';
import ReactDOM from 'react-dom';
import reactToString from 'react-to-string';
import Prism from 'prismjs/components/prism-core';
require('prismjs');
require('prismjs/themes/prism.css');
import reactElementToJSXString from 'react-element-to-jsx-string';
import {ButtonsContained} from '../Buttons/ButtonsExample'
import {LiveProvider, LiveEditor, LiveError, LivePreview} from 'react-live'
import {
    Card,
    CardBody,
    CardTitle,
    CardText,
    CardHeader,
    CardFooter,
    Button,
    TabContent,
    TabPane,
    Nav,
    NavItem,
    NavLink,
    Row,
    Col
} from 'reactstrap';
import LiveCode from '../../components/LiveCode';
import PropTypes from 'prop-types';
import * as DsComponents from '../../../ds-components/';

class ComponentDemoMd extends Component {
    constructor(props) {
        super(props);

        this.toggle = this
            .toggle
            .bind(this);
        this.state = {
            activeTab: '1'
        };
    }
    componentDidMount() {
        Prism.highlightAll();
    };
    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({activeTab: tab});
        }
    }

    render() {
        const {code, children} = this.props;
        let demoCode;

        if (code) {
            console.log(code)
            demoCode = code
        } else {
            demoCode = reactElementToJSXString((
                <React.Fragment>{this.props.children}</React.Fragment>
            ), {showFunctions: true})
        }

        return (
            <div className='demo prism-section'>
                <Card>
                    <CardHeader className='component-demo-box'>
                        <div className="row mx-auto">
                            {this.props.children}
                        </div>

                    </CardHeader>
                    <CardBody className='demo-card-body'>
                        <Nav tabs>
                            <NavItem>
                                <NavLink
                                    id='leftTab'
                                    className={classnames({
                                    active: this.state.activeTab === '1'
                                })}
                                    onClick={() => {
                                    this.toggle('1');
                                }}>
                                    React
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    className={classnames({
                                    active: this.state.activeTab === '2'
                                })}
                                    onClick={() => {
                                    this.toggle('2');
                                }}>
                                    HTML
                                </NavLink>
                            </NavItem>
                        </Nav>
                        <TabContent activeTab={this.state.activeTab}>
                            <TabPane tabId="1">

                                <code className='language-jsx'>
                                    {demoCode}
                                </code>

                            </TabPane>

                            <TabPane tabId="2">

                                <code className="language-markup">
                                    <pre> {ReactDOMServer.renderToStaticMarkup(children, {showFunctions: true})}</pre>
                                </code>
                            </TabPane>
                        </TabContent>
                    </CardBody>

                </Card>
            </div>
        );
    }
}

export default ComponentDemoMd;