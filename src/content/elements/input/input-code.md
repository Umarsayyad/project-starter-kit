---
path: /elements/input/code
layout: custom
title: Text Inputs
description: Input
group: elements
tabs: ['Usage', 'Code', 'Style']
---

# Code

### Text Inputs

<component-demo>
<label htmlFor="formGroupExampleInput">Default
</label>
<input type="text" className="form-control" id="formGroupExampleInput"
placeholder="Form Control Regular"></input></component-demo>

---

<component-demo>
<component-live component='button' className='btn-outline-primary'>button</component-live>
</component-demo>
### Label Options

<component-demo code=' <div className="form-row align-items-center pb-3"> <div className="col-xs-12 col-sm-1 text-xs-left text-sm-right">
                            <Label for="exampleEmail">Label</Label>
                        </div>
                        <div className="col-10">
                            <Input type="text" placeholder="Placeholder text"/>
                        </div>
    </div>' ><form-inline></form-inline></component-demo>

---

### Required vs Optional

<component-demo><form-optional></form-optional></component-demo>

---

### Validation

<component-demo><form-validation></form-input></component-demo>

---

### Variants

<component-demo><input-icon></input-icon></component-demo>

---
