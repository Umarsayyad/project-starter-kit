import React from 'react'
import markdownIt from 'markdown-it'

const Section = ({title, description, children}) => {
  const md = markdownIt({html: true, linkify: true})

  return (
    <section className="prism-section">
      <header className="prism-section__header">
        <h2
          className="prism-section__title"
          dangerouslySetInnerHTML={{
          __html: title
        }}/> {description && (<div
          className="prism-section__description"
          dangerouslySetInnerHTML={{
          __html: md.render(description)
        }}/>)}
      </header>
      <div className="prism-section__body">{children}</div>
    </section>
  )
}

export default Section
