import React from 'react';

export const CBradioExample = () => (
    <div classname="card-body bg-white p-16">
        <div className="input-group mb-3">
            <div className="input-group-prepend">
                <div className="input-group-text">
                    <label className='checkbox--custom'>
                        <input type="checkbox" name="check1"/> {' '}
                    </label>
                </div>
            </div>
            <input
                type="text"
                className="form-control"
                aria-label="Text input with checkbox"
                placeholder="Text input with checkbox"/>
        </div>
        <div className="input-group">
            <div className="input-group-prepend">
                <div className="input-group-text">
                    <input type="radio" aria-label="Radio button for following text input"/>
                </div>
            </div>
            <input
                type="text"
                className="form-control"
                aria-label="Text input with radio button"
                placeholder="Text input with radio button"/>
        </div>
    </div>

)
