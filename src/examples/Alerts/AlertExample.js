import React from 'react';

export const AlertExample = () => (

    <div classname="demo-alerts">
        <div className="alert alert-primary" role="alert">
            <i className="icon prs-icon-info"/>
            <span className="alert__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
        </div>
        <div className="alert alert-primary" role="alert">
            <i className="icon prs-icon-info"/>
            <div className="alert__message">
                <span className="alert__title">Info/Neutral Notification</span>
                <br/>
                <span className="alert__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
            </div>
        </div>
        <hr/>
        <div className="alert alert-success" role="alert">
            <i className="icon prs-icon-info"/>
            <span className="alert__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
        </div>
        <div className="alert alert-success" role="alert">
            <i className="icon prs-icon-info"/>
            <div className="alert__message">
                <span className="alert__title">Success/Validation</span>
                <br/>
                <span className="alert__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
            </div>
        </div>
        <hr/>
        <div className="alert alert-warning" role="alert">
            <i className="icon prs-icon-info"/>
            <span className="alert__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
        </div>
        <div className="alert alert-warning" role="alert">
            <i className="icon prs-icon-info"/>
            <div className="alert__message">
                <span className="alert__title">Alert/Warning</span>
                <br/>
                <span className="alert__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
            </div>
        </div>
        <hr/>
        <div className="alert alert-danger" role="alert">
            <i className="icon prs-icon-info"/>
            <span className="alert__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
        </div>
        <div className="alert alert-danger" role="alert">
            <i className="icon prs-icon-info"/>
            <div className="alert__message">
                <span className="alert__title">Error/Failure</span>
                <br/>
                <span className="alert__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
            </div>
        </div>
    </div>

)
