import React, {Component} from "react";
import {LiveProvider, LiveEditor, LiveError, LivePreview} from "react-live";
import styled, {css} from 'styled-components'
import FunctionalComponent from './FunctionalComponent.js'
import Button from './Button.js';
import jsxToString from 'jsx-to-string';
import Highlight, {defaultProps} from "prism-react-renderer";

export default class LiveCode extends Component {
    render() {

        return (

            <Highlight
                {...defaultProps}
                code={this.props.codeString}
                language={this.props.language}>
                {({className, style, tokens, getLineProps, getTokenProps}) => (
                    <pre className={className} style={style}>
                {tokens.map((line, i) => (
                  <div {...getLineProps({ line, key: i })}>
                    {line.map((token, key) => (
                      <span {...getTokenProps({ token, key })} />
                    ))}
                  </div>
                ))}
              </pre>
                )}
            </Highlight>
        )

    }
}
