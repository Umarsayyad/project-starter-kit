import React, {Component} from 'react'
import {graphql} from 'gatsby'
import Helmet from 'react-helmet'
import classnames from 'classnames';
import {FaChevronLeft, FaChevronRight} from 'react-icons/fa'
import Layout from '../components/layout'
import Link from '../components/Link'
import Tags from '../components/Tags'
import ComponentDetailHeader from '../components/ComponentDetailHeader'
import Tab from '../components/Tab'
import Tabs from '../components/Tabs'
import ButtonLink from '../components/ButtonLink'
import Button from '../components/Button'
import ComponentExample from '../components/ComponentExample'
import markdownIt from 'markdown-it'
import ColorCard from '../components/ColorCard'
import rehypeReact from 'rehype-react'
import {
    FlexGroup,
    MdTable,
    p,
    h1,
    h4,
    imgCaption,
    h1Condensed
} from '../components/Markdown/Markdown'
import TypographyTable from '../components/Typography/TypographyTable'
import AccordionExample from '../examples/Accordion/AccordionExample'
import LiveCode from '../components/LiveCode.js';
import PrismIcons from '../components/PrismIcons.js';

const renderAst = new rehypeReact({
    createElement: React.createElement,
    components: {
        "flex-group": FlexGroup,
        "color-card": ColorCard,
        "typography-table": TypographyTable,
        "md-table": MdTable,
        "prism-accordion": AccordionExample,
        "p": p,
        "h1": h1,
        "h4": h4,
        "h1-condensed": h1Condensed,
        'prism-icons': PrismIcons,
        'img-caption': imgCaption
    }
}).Compiler

export class Foundation extends Component {
    componentDidMount() {}

    render() {

        const md = markdownIt({html: true, linkify: true})
        const {markdownRemark: post} = this.props.data
        const {next, prev} = this.props.pageContext

        return (

            <Layout>
                <Helmet title={`${post.frontmatter.title}`}/>
                <ComponentDetailHeader
                    title={post.frontmatter.title}
                    description={post.frontmatter.description}
                    status={post.frontmatter.status}
                    group={post.frontmatter.group}/> {post.frontmatter.tabs && (<Tabs tabs={post.frontmatter.tabs} styleModifier='component-tabs'/>)}
                <div className='component-tab'>
                    <div className="l-container markdown">
                        {renderAst(post.htmlAst)}
                    </div>
                </div>

            </Layout>
        )
    }
}

export default Foundation

export const FoundationQuery = graphql `
  query foundationQuery($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      htmlAst
      frontmatter {
        path
        tags
        title
        component
        description
        status
        group
        subgroup
        tabs 
        role
        usage {
          title
          description
          demo
        }
      }
    }
  }
`
