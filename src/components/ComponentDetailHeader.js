import React from 'react'
import Badge from './Badge'
import markdownIt from 'markdown-it'

const ComponentDetailHeader = ({group, title, status}) => {
    const md = markdownIt({html: true, linkify: true})
    return (
        <div className="prism-page-header comp-detail-header">
            <div className="container" id='prism-page-header__container'>
                <div className="row ">
                    <div className="col">
                        {group && <p className="prism-page-header__kicker">{group
                                .charAt(0)
                                .toUpperCase() + group.slice(1)}</p>}
                        <h1 className="prism-page-header__title">
                            {title}{' '} {status && status !== 'Complete'
                                ? <Badge status={status}/>
                                : null}
                        </h1>

                    </div>

                </div>
            </div>

        </div>
    )
}

export default ComponentDetailHeader
