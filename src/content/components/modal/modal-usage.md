---
layout: custom
group: components
path: /components/modal/usage
title: Modal
component: Modal
heading: Modal
role: main
description: Modals
tabs: [Usage, Code, Style]
---

#modal usage

<usage-demo>
<modal-heading></modal-heading>
 <modal-noheading></modal-noheading>
</usage-demo>
